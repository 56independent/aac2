# include <iostream>
# include <fstream>

int main(){
	int filenameint = 1;
	int repeats = 100000; // edit this line to change how many files to use
	repeats += 2; // make sure that there really are repeats amount of files (if it was set to 100, then there would be 99 files)
	while (repeats >=  filenameint){
		// turns an int into string, then adds a filename to it
		std::string filenamestr; 
		filenamestr += "./files/"; // comment out this line to put the files in the same directory as this program 
		filenamestr += std::to_string(filenameint);
		filenamestr += ".txt";

		// creates file, fills, and closes
		std::ofstream file;
		file.open(filenamestr);
		file << "FILE FLOOD SHALL FLOOD UR DIRECTORY";
		file.close();

		//increments the integer filename
		filenameint++;
	}
}
