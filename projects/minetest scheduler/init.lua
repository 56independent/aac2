-- Time to shut down server.
local H, M = 23, 30

local timer = 0
minetest.register_globalstep(function(dtime)
	timer = timer + dtime
	if timer < 1 then return end
	timer = 0
	local t = os.date("*t")
	if (t.hour == H) and (t.min == M) and (t.sec <= 5) then
		minetest.chat_send_all("Scheduled shutdown. "
				.."Please come back in a few minutes.")
		minetest.after(2, minetest.request_shutdown)
	end
end)
