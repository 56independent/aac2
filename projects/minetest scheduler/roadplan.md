# short
to make an automatic scheduler that will schedule the opening and closing of a few minetest servers

# full-on details
i want to schedule the opening and closing of servers. this can probably be executed via bash alone. this can be done with one master script, that automates the starting and shutting down of minetest servers. cron can pass info to the bash script, with the use of the `echo "data" >> filename` feature. i can imagine sending data along multiple files, like `worldname.txt`, <!-- todo -->

# reasons 
i want to run 4 minetest worlds, under one server. i can have everyone play around, until the server shuts down (after plenty of warning). then, 10 minutes later, the server starts up again, but in a different world.

this can help me improve my reputation in the minetest community, where i could be a kind admin. this could also help me down the path to being a sysadmin or coder, as a project to add to my portfolio (the `56th server`). it may die before i can show any intrested employers, instead reling on video and archives to show. 

# potential problems 
i could face many problems on my journey. here are some i could have, and potential fixes:

----------------------------------------------------------------------------------
|problem                                    |potential fix                       |
----------------------------------------------------------------------------------
|the server crashing, and me not knowing why|                                    |
----------------------------------------------------------------------------------
|terrible, terrible lag                     |upgrade server and internet         |
----------------------------------------------------------------------------------
|getting told off by mum for breaking wifi  |move to professional server, or kill|
----------------------------------------------------------------------------------
|cron not working                           |lots and lots of testing            | ---------------------------------------------------------------------------------- 
|world corruption                           |restore backup from yesterday       |
----------------------------------------------------------------------------------
|world and backup corruption                |PANIC!!                             |
----------------------------------------------------------------------------------

# problems
problems that should arise are to be recorded here.

# to-do list 
- find out how to launch and shutdown mt servers from the terminal
- create a "profile" for  the online stuff. 
- sync player data across the sever (but not inventories and locked chests - they could be messed up) (will need some sql knowladge?)
- make the "backbone" script. it can operate as the backbone, with helpful code snippets, and directions to other code bits. 
- make CRON ACTUALLY WORK (maybe it wont work while the computer is *sleeping*)
- make a multitude of scripts, that can eb executed by cron. maybe one script, where you pass a paramter to them?

