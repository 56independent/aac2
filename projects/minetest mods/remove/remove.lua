function removeItem(item) do
	for name, def in ipairs(minetest.registered_items) do
		if name:find("halloween:") == 1 then
			def.groups.not_in_creative_inventory = 1
			minetest.override_item(item, def)
		end
	end
end

removeItem("adtrains:tcb")
removeItem("advtrains)luaautomation:dtrack_placer")
removeItem("advtrains_luaautomation:oppanel")

removeItem("apartment:build_chest")

removeItem("symmmetool:mirror")

removeItem("mapserver:border")
removeItem("mapserver:label")
removeItem("mapserver:poi_blue")
removeItem("mapserver:poi_green")
removeItem("mapserver:poi_orange")
removeItem("mapserver:poi_purple")
removeItem("mapserver:poi_red")
removeItem("mapserver:train")
