-- GENERATED CODE
-- Node Box Editor, version 0.9.0
-- Namespace: test

minetest.register_node("test:node_1", {
	tiles = {
		"default_wood.png",
		"default_wood.png",
		"sign_forward.png",
		"default_wood.png",
		"default_wood.png",
		"sign_forward.png"
	},
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.1875, -0.5, -0.375, 0.1875, 0.5}, -- NodeBox1
		}
	}
})

