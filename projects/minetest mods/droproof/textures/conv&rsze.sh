#!/bin/bash

# Takes image, converts it and resizes it.

convert $1 $2.png   
mogrify -resize 16x16! $2.png

convert -composite stain.png $2.png $2stain.png

cp $2.png support$2.png
cp $2stain.png support$2stain.png
