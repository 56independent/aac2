nodeboxesMixed = {
    tile = {
        {-0.5, -0.4375, -0.5, 0.5, -0.375, 0.5}, -- NodeBox1
        {-0.4375, -0.5, -0.4375, 0.4375, -0.4375, 0.4375}, -- NodeBox2
    },

    tilepipe = {
			{-0.0625, -0.375, -0.5, 0.0625, -0.25, 0.5}, -- NodeBox3
            {-0.5, -0.4375, -0.5, 0.5, -0.375, 0.5}, -- NodeBox1
        {-0.4375, -0.5, -0.4375, 0.4375, -0.4375, 0.4375}, -- NodeBox2
    },

    tilemesecon = {
        {-0.0625, -0.375, -0.5, 0.0625, -0.3125, 0.5}, -- NodeBox3
        {-0.5, -0.4375, -0.5, 0.5, -0.375, 0.5}, -- NodeBox1
        {-0.4375, -0.5, -0.4375, 0.4375, -0.4375, 0.4375}, -- NodeBox2
    },

    tilemeseconmesecon = {
        {-0.125, -0.375, -0.5, 0.125, -0.3125, 0.5}, -- NodeBox3
        {-0.5, -0.4375, -0.5, 0.5, -0.375, 0.5}, -- NodeBox1
        {-0.4375, -0.5, -0.4375, 0.4375, -0.4375, 0.4375}, -- NodeBox2
    },

    tilepipepipe = {
        {-0.125, -0.375, -0.5, 0.125, -0.25, 0.5}, -- NodeBox3
        {-0.5, -0.4375, -0.5, 0.5, -0.375, 0.5}, -- NodeBox1
        {-0.4375, -0.5, -0.4375, 0.4375, -0.4375, 0.4375}, -- NodeBox2
    },

    tilepipemesecon = {
        {-0.125, -0.375, -0.5, 0.125, -0.3125, 0.5}, -- NodeBox3
        {-0.125, -0.375, -0.5, 0, -0.25, 0.5}, -- NodeBox4
        {-0.5, -0.4375, -0.5, 0.5, -0.375, 0.5}, -- NodeBox1
        {-0.4375, -0.5, -0.4375, 0.4375, -0.4375, 0.4375}, -- NodeBox2
    },

    tilepipepipemeseconmesecon = {
			{-0.125, -0.375, -0.5, 0.25, -0.3125, 0.5}, -- NodeBox3
			{-0.3125, -0.375, -0.5, -0.0625, -0.25, 0.5}, -- NodeBox4
            {-0.5, -0.4375, -0.5, 0.5, -0.375, 0.5}, -- NodeBox1
        {-0.4375, -0.5, -0.4375, 0.4375, -0.4375, 0.4375}, -- NodeBox2
    },

    tilepipepipemesecon = {
        {-0.125, -0.375, -0.5, 0.25, -0.3125, 0.5}, -- NodeBox3
        {-0.125, -0.375, -0.5, 0.125, -0.25, 0.5}, -- NodeBox4
        {-0.5, -0.4375, -0.5, 0.5, -0.375, 0.5}, -- NodeBox1
        {-0.4375, -0.5, -0.4375, 0.4375, -0.4375, 0.4375}, -- NodeBox2
    },

    tilepipemeseconmesecon = {
        {-0.125, -0.375, -0.5, 0.25, -0.3125, 0.5}, -- NodeBox3
        {-0.125, -0.375, -0.5, 0, -0.25, 0.5}, -- NodeBox4
        {-0.5, -0.4375, -0.5, 0.5, -0.375, 0.5}, -- NodeBox1
        {-0.4375, -0.5, -0.4375, 0.4375, -0.4375, 0.4375}, -- NodeBox2
    },

    supports = {
        {-0.5, -0.5, -0.5, -0.4375, 0.5, -0.4375}, -- NodeBox6
        {-0.5, -0.5, 0.4375, -0.4375, 0.5, 0.5}, -- NodeBox7
        {0.4375, -0.5, 0.4375, 0.5, 0.5, 0.5}, -- NodeBox8
        {0.4375, -0.5, -0.5, 0.5, 0.5, -0.4375}, -- NodeBox9
    },

    tileshortsupports = {
        {-0.5, -0.4375, -0.5, -0.4375, 0.5, -0.4375}, -- NodeBox6
        {-0.5, -0.4375, 0.4375, -0.4375, 0.5, 0.5}, -- NodeBox7
        {0.4375, -0.4375, 0.4375, 0.5, 0.5, 0.5}, -- NodeBox8
        {0.4375, -0.4375, -0.5, 0.5, 0.5, -0.4375}, -- NodeBox9
        {-0.5, -0.4375, -0.5, 0.5, -0.375, 0.5}, -- NodeBox1
        {-0.4375, -0.5, -0.4375, 0.4375, -0.4375, 0.4375}, -- NodeBox2
    },

    tileendon = {
        {-0.0625, -0.375, -0.5, 0.0625, -0.3125, 0.125}, -- NodeBox3
        {-0.5, -0.4375, -0.5, 0.5, -0.375, 0.5}, -- NodeBox1
        {-0.4375, -0.5, -0.4375, 0.4375, -0.4375, 0.4375}, -- NodeBox2

    },
}

function make_tile_registers_node(name, textures, description, connections, nodebox, binary) -- Connections: {2, "Diglines"} -- Straight digilines, {3, "mesecon", "pipework"} -- T junction for both mesecons and pipeworks. 
    minetest.register_node("droproof:" .. name, {
    side = binary .. "_side.png",
    tiles = {"toptemplate.png", textures .. ".png", side, side, side, side}, -- up (+Y), down (-Y), right (+X), left (-X), back (+Z), front (-Z).
    
    walkable = true,
    description = description,
    groups = {
        cracky=3,
        --save_in_at_nodedb=2,
    },
    
    paramtype2 = "facedir",
    
    drawtype = "nodebox",
    paramtype = "light",
    node_box = {
    type = "fixed",
    fixed = nodebox,
    }
    })
end

function make_tile(name, description, type, nodebox, binary) -- Type 1 has all types of connection, 2 has straight and end-on, 3 has no conductors
    make_tile_registers_node(name, name, description, {}, nodebox, binary) -- Redundant now, but awaiting features.
    -- make_tile_registers_node(name .. "supported", description .. ", with supports", {}, nodebox + nodeboxesMixed.tileshortsupports) -- Waiting until we can concatenate tables together.
end

make_tile("clean", "Clean tile", 1, nodeboxesMixed.tile, "clean")
make_tile("cleanstain", "Clean, but stained tile", 1, nodeboxesMixed.tile, "clean")


make_tile("dirty", "dirty tile", 1, nodeboxesMixed.tile, "dirty")
make_tile("dirtystain", "dirty, and stained tile", 1, nodeboxesMixed.tile, "dirty")

make_tile("supportclean", "Clean tile, with supports", 1, nodeboxesMixed.tileshortsupports, "clean")
make_tile("supportcleanstain", "Clean, but stained tile, with supports", 1, nodeboxesMixed.tileshortsupports, "clean")


make_tile("supportdirty", "dirty tile, with supports", 1, nodeboxesMixed.tileshortsupports, "dirty")
make_tile("supportdirtystain", "dirty, and stained tile, with supports", 1, nodeboxesMixed.tileshortsupports, "dirty")

minetest.register_node("droproof:supports", {
    tiles = {"supports.png", "supports.png", "side_supports.png", "side_supports.png", "side_supports.png", "side_supports.png" }, -- up (+Y), down (-Y), right (+X), left (-X), back (+Z), front (-Z).
    
    walkable = true,
    description = "Supports",
    groups = {
        cracky=3,
        --save_in_at_nodedb=2,
    },
    
    paramtype2 = "facedir",
    
    drawtype = "nodebox",
    paramtype = "light",
    node_box = {
    type = "fixed",
    fixed = nodeboxesMixed.supports,
    }
})