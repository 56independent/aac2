# Introduction
Dropped ceilings offer extreme benifts in term of in-structure infrastructure. Thanks to this mod, a ceiling void is no longer required to place mesecons, diglines, pipeworks, or water systems. This mod also offers walkways (best used with factory_bridges). 

# Nodes
This mod offers many nodes which establish dropped ceilings.

# Tiles
There are many tiles offered with different uses.

* Standard tile

Utility tiles:

* Light square tile
* Fluorescent Light tile
* Alarm tile (For use with fire_safety)
* Wire supports to floor above
* HVAC intake/outake vent
* Inset Fluorescent Lighting
* Path tiles

All the standard tiles come in three formats, with one being high-quality, the second being poor quality, and the third being stained versions of both of these.

In the future, the tiles won't be static and will allow this:

These also support any combination of:

* Mesecons
* Digilines
* Pipeworks
* Waterworks

Each of these are a flat cuboid stretched across the tiles. These can have each of these junctions:

* Straight
* T-junction
* 4-way junction
* Crossing

All of these conduct to the block directly above or below. 

All the lighting has end-on wiring, and alarm tiles only support diglines. Path tiles do not conduct anything. 


