firesafety = {}
--[[
fire safety mod, just in case of fire.
this file contains:
the blocks, with crafting recipes and all
]]
--[[fuctions]]
--[sending messages]
function sendFireMessage()
	digiline:receptor_send(pos,digiline.rules.default,channel,"fire")
end


-- [[fire signs]]
--[nodes}
--forwards
local signlightoutput = 7
minetest.register_node("firesafety:sign_forward",{
	description = "fire sign, forwards"
	light_source = signlightoutput
})

--left
minetest.register_node("firesafety:sign_left",{
	description = "fire sign, left"
	light_source = signlightoutput

})

--right
minetest.register_node("firesafety:sign_right",{
	description = "fire sign, right"
	light_source = signlightoutput
})

--[crafting recipe]
--forwards
minetest.register_craft({
	output = "firesafety:sign_forward 2"
	recipe = {
	{"dye:green","default:torch","dye:green"}
	{"default:torch","default:torch","default:torch"}
	{"default:wood 2","default:torch","default:wood 2"}
	}
})

--left
minetest.register_craft({
	output = "firesafety:sign_left 2"
	recipe = {
	{"dye:green","default:torch","default:wood 2"}
	{"default:torch","default:torch","default:torch"}
	{"dye:green","default:torch","default:wood 2"}
	}
})

--right
minetest.register_craft({
	output = "firesafety:sign_right 2"
	recipe = {
	{"default:wood 2","default:torch","dye:green"}
	{"default:torch","default:torch","default:torch"}
	{"default:wood 2","default:torch","dye:green"}
	}
})

--[[heat detector
sends the message fire, to be read by alarms. detects fire in a 15 block 
radius
]]
minetest.register_node("firesafety:heatdetector",{
	local node_pos   = minetest.find_node_near(pos, 5, { "fire:basic_flame" })
	if node_pos then
		sendFireMessage()_
	end
})

minetest.register_craft({
	output = "firesafety:heatdetector 10"
	recipe = {
	{"basic_materials:plastic_sheet","digilines:digiline 4","basic_materials:plastic_sheet"}
	{"default:steel_ingot 2","mesecons:luacontroller","default:steel_ingot 2"}
	{"basic_materials:plastic_sheet","bucket:bucket_water","basic_materials:plastic_sheet"}
	}

--[[alarm
upon reciving the message "fire" will play an obnoxious sound, forcing 
all players who want to keep their eardrums intact to leave the building
]]
minetest.register_node("firesaftey:alarm",{
	--when  we recive message then
		--play sound 
		{
			name = "firesafety_alarmsound.ogg"
			pos = {x = 1, y = 2, z = 3},
			gain = 1.5, -- default
			max_hear_distance = 20, -- default, uses an euclidean metric
		}
	end
