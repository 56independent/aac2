-- Node scripts - not only for nodes!

minetest.register_node("testing:really_long_unweildy_item_name_look_how_long_i_am_try_me_worldedit", {
    description = "Unweildy name describing this node with very much misplgns and to distract you",
    tiles = {"block.png"},
    groups = {cracky=1,oddly_breakable_by_hand=1,explody=1},
})

minetest.register_craftitem("testing:maybe_dont_eat_that", {
    description = "Mouldy stone",
    inventory_image = "mouldy_stone.png",
    on_use = minetest.item_eat(-2),
})

minetest.register_craft({
    type = "cooking",
    output = "testing:maybe_dont_eat_that",
    recipe = "default:stonebrick",
    cooktime = 1,
})

minetest.register_craft({
    type = "fuel",
    recipe = "testing:maybe_dont_eat_that",
    burntime = 5000,
})

minetest.register_node("testing:broken_barrier",{
    description = "Barrier, but it is BROKEN",
    inventory_image = "bars.png",
    paramtype2 = "wallmounted",
    drawtype = "airlike",
    sunlight_propagates = true,
    walkable = false,
    pointable = true,
    buildable_to = false,
})

minetest.register_node("testing:rluinlhliatmw_slope", {
    description = "Unweildy name describing this node with very much misplgns and to distract you, with a slope",
    tiles = {"block.png"},
    drawtype = "nodebox",
    groups = {cracky=1,oddly_breakable_by_hand=1,explody=1},
    node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5, -0.3125, 0.5, 0.5}, -- NodeBox1
			{-0.3125, -0.5, -0.5, -0.1875, 0.3125, 0.5}, -- NodeBox2
			{-0.1875, -0.5, -0.5, 0, 0.1875, 0.5}, -- NodeBox3
			{-0.0625, -0.5, -0.5, 0.125, 0.0625, -0.25}, -- NodeBox4
		}
	}
})

minetest.register_craftitem("testing:cyanide_pill", {
    description = "Cyanide Pill (for suicide)",
    inventory_image = "pill.png",
    on_use = function()
        minetest.item_eat(-20)
        minetest.chat_send_all("Someone killed themself!")
    end,
    
})