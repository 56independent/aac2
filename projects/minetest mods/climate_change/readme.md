# purpose of the mod
this mod was made as a half joke post on discord. now im really making it. this mod just emulates climate change, but a lot quicker. a well-played minetest server could have the sea rise by 0.5 m per day.

# some instructions
this mod raises the water by 1 m every 100 days (should be changable in minetest.conf). to stop the water rise, you need to find a block with "politics" written on it. once you remove it, you get a stack  of some item (default is mese, but can be changed). there is also a block with Greta Thunberg's face on it. if you mine this block, the END OF THE WORLD happens.

## the END OF THE WORLD
this is a certain state of gameplay where the world has ENDED. everybody is automatically permabanned (you can change what happens in the code - i dont want to add a minetest.conf setting, too complex). the reason is simply "Greta is very angry you didnt remove the block".

# minetest.conf
here follows a list of settings, their possible types, and description:

- rise - int - controls the amount of water rise per `term`. default is 1
- term - int - the amount of time in in-game days until the water raises by `rise`
- height - int - the lowest height that the `politics` and `Greta` block  spawns at. default is -20
- range - int - how far away from `0,0,0` the `politics` and `Greta` block can spawn at. default is 500
- reason - string - the reason you are permabanned. the default is "Greta is very angry you didnt remove the block"