minetest.register_biome({
    name = "diamond",
    node_top = "richbiomes:diamond_dust",
    depth_top = 2,
    node_filler = "default:stone_with_diamond",
    node_stone = "default:diamondblock",
    depth_filler = 3,
    y_max = 1000,
    y_min = -3,
    heat_point = 50,
    humidity_point = 50,
})


--[[{
        name = "tundra",

        node_dust = "default:snow",
        -- Node dropped onto upper surface after all else is generated

        node_top = "default:dirt_with_snow",
        depth_top = 1,
        -- Node forming surface layer of biome and thickness of this layer

        node_filler = "default:permafrost",
        depth_filler = 3,
        -- Node forming lower layer of biome and thickness of this layer

        node_stone = "default:bluestone",
        -- Node that replaces all stone nodes between roughly y_min and y_max.

        node_water_top = "default:ice",
        depth_water_top = 10,
        -- Node forming a surface layer in seawater with the defined thickness

        node_water = "",
        -- Node that replaces all seawater nodes not in the surface layer

        node_river_water = "default:ice",
        -- Node that replaces river water in mapgens that use
        -- default:river_water

        node_riverbed = "default:gravel",
        depth_riverbed = 2,
        -- Node placed under river water and thickness of this layer

        node_cave_liquid = "default:lava_source",
        node_cave_liquid = {"default:water_source", "default:lava_source"},
        -- Nodes placed inside 50% of the medium size caves.
        -- Multiple nodes can be specified, each cave will use a randomly
        -- chosen node from the list.
        -- If this field is left out or 'nil', cave liquids fall back to
        -- classic behaviour of lava and water distributed using 3D noise.
        -- For no cave liquid, specify "air".

        node_dungeon = "default:cobble",
        -- Node used for primary dungeon structure.
        -- If absent, dungeon nodes fall back to the 'mapgen_cobble' mapgen
        -- alias, if that is also absent, dungeon nodes fall back to the biome
        -- 'node_stone'.
        -- If present, the following two nodes are also used.

        node_dungeon_alt = "default:mossycobble",
        -- Node used for randomly-distributed alternative structure nodes.
        -- If alternative structure nodes are not wanted leave this absent for
        -- performance reasons.

        node_dungeon_stair = "stairs:stair_cobble",
        -- Node used for dungeon stairs.
        -- If absent, stairs fall back to 'node_dungeon'.

        y_max = 31000,
        y_min = 1,
        -- Upper and lower limits for biome.
        -- Alternatively you can use xyz limits as shown below.

        max_pos = {x = 31000, y = 128, z = 31000},
        min_pos = {x = -31000, y = 9, z = -31000},
        -- xyz limits for biome, an alternative to using 'y_min' and 'y_max'.
        -- Biome is limited to a cuboid defined by these positions.
        -- Any x, y or z field left undefined defaults to -31000 in 'min_pos' or
        -- 31000 in 'max_pos'.

        vertical_blend = 8,
        -- Vertical distance in nodes above 'y_max' over which the biome will
        -- blend with the biome above.
        -- Set to 0 for no vertical blend. Defaults to 0.

        heat_point = 0,
        humidity_point = 50,
        -- Characteristic temperature and humidity for the biome.
        -- These values create 'biome points' on a voronoi diagram with heat and
        -- humidity as axes. The resulting voronoi cells determine the
        -- distribution of the biomes.
        -- Heat and humidity have average values of 50, vary mostly between
        -- 0 and 100 but can exceed these values.
    }
    
    {
        deco_type = "simple",

        place_on = "default:dirt_with_grass",
        -- Node (or list of nodes) that the decoration can be placed on

        sidelen = 8,
        -- Size of the square divisions of the mapchunk being generated.
        -- Determines the resolution of noise variation if used.
        -- If the chunk size is not evenly divisible by sidelen, sidelen is made
        -- equal to the chunk size.

        fill_ratio = 0.02,
        -- The value determines 'decorations per surface node'.
        -- Used only if noise_params is not specified.
        -- If >= 10.0 complete coverage is enabled and decoration placement uses
        -- a different and much faster method.

        noise_params = {
            offset = 0,
            scale = 0.45,
            spread = {x = 100, y = 100, z = 100},
            seed = 354,
            octaves = 3,
            persistence = 0.7,
            lacunarity = 2.0,
            flags = "absvalue"
        },
        -- NoiseParams structure describing the perlin noise used for decoration
        -- distribution.
        -- A noise value is calculated for each square division and determines
        -- 'decorations per surface node' within each division.
        -- If the noise value >= 10.0 complete coverage is enabled and
        -- decoration placement uses a different and much faster method.

        biomes = {"Oceanside", "Hills", "Plains"},
        -- List of biomes in which this decoration occurs. Occurs in all biomes
        -- if this is omitted, and ignored if the Mapgen being used does not
        -- support biomes.
        -- Can be a list of (or a single) biome names, IDs, or definitions.

        y_min = -31000,
        y_max = 31000,
        -- Lower and upper limits for decoration.
        -- These parameters refer to the Y co-ordinate of the 'place_on' node.

        spawn_by = "default:water",
        -- Node (or list of nodes) that the decoration only spawns next to.
        -- Checks two horizontal planes of 8 neighbouring nodes (including
        -- diagonal neighbours), one plane level with the 'place_on' node and a
        -- plane one node above that.

        num_spawn_by = 1,
        -- Number of spawn_by nodes that must be surrounding the decoration
        -- position to occur.
        -- If absent or -1, decorations occur next to any nodes.

        flags = "liquid_surface, force_placement, all_floors, all_ceilings",
        -- Flags for all decoration types.
        -- "liquid_surface": Instead of placement on the highest solid surface
        --   in a mapchunk column, placement is on the highest liquid surface.
        --   Placement is disabled if solid nodes are found above the liquid
        --   surface.
        -- "force_placement": Nodes other than "air" and "ignore" are replaced
        --   by the decoration.
        -- "all_floors", "all_ceilings": Instead of placement on the highest
        --   surface in a mapchunk the decoration is placed on all floor and/or
        --   ceiling surfaces, for example in caves and dungeons.
        --   Ceiling decorations act as an inversion of floor decorations so the
        --   effect of 'place_offset_y' is inverted.
        --   Y-slice probabilities do not function correctly for ceiling
        --   schematic decorations as the behaviour is unchanged.
        --   If a single decoration registration has both flags the floor and
        --   ceiling decorations will be aligned vertically.

        ----- Simple-type parameters

        decoration = "default:grass",
        -- The node name used as the decoration.
        -- If instead a list of strings, a randomly selected node from the list
        -- is placed as the decoration.

        height = 1,
        -- Decoration height in nodes.
        -- If height_max is not 0, this is the lower limit of a randomly
        -- selected height.

        height_max = 0,
        -- Upper limit of the randomly selected height.
        -- If absent, the parameter 'height' is used as a constant.

        param2 = 0,
        -- Param2 value of decoration nodes.
        -- If param2_max is not 0, this is the lower limit of a randomly
        -- selected param2.

        param2_max = 0,
        -- Upper limit of the randomly selected param2.
        -- If absent, the parameter 'param2' is used as a constant.

        place_offset_y = 0,
        -- Y offset of the decoration base node relative to the standard base
        -- node position.
        -- Can be positive or negative. Default is 0.
        -- Effect is inverted for "all_ceilings" decorations.
        -- Ignored by 'y_min', 'y_max' and 'spawn_by' checks, which always refer
        -- to the 'place_on' node.

        ----- Schematic-type parameters

        schematic = "foobar.mts",
        -- If schematic is a string, it is the filepath relative to the current
        -- working directory of the specified Minetest schematic file.
        -- Could also be the ID of a previously registered schematic.

        schematic = {
            size = {x = 4, y = 6, z = 4},
            data = {
                {name = "default:cobble", param1 = 255, param2 = 0},
                {name = "default:dirt_with_grass", param1 = 255, param2 = 0},
                {name = "air", param1 = 255, param2 = 0},
                 ...
            },
            yslice_prob = {
                {ypos = 2, prob = 128},
                {ypos = 5, prob = 64},
                 ...
            },
        },
        -- Alternative schematic specification by supplying a table. The fields
        -- size and data are mandatory whereas yslice_prob is optional.
        -- See 'Schematic specifier' for details.

        replacements = {["oldname"] = "convert_to", ...},

        flags = "place_center_x, place_center_y, place_center_z",
        -- Flags for schematic decorations. See 'Schematic attributes'.

        rotation = "90",
        -- Rotation can be "0", "90", "180", "270", or "random"

        place_offset_y = 0,
        -- If the flag 'place_center_y' is set this parameter is ignored.
        -- Y offset of the schematic base node layer relative to the 'place_on'
        -- node.
        -- Can be positive or negative. Default is 0.
        -- Effect is inverted for "all_ceilings" decorations.
        -- Ignored by 'y_min', 'y_max' and 'spawn_by' checks, which always refer
        -- to the 'place_on' node.
    }
]]--