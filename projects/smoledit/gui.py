def gui():
    from PyQt5.QtWidgets import *
    app = QApplication([])
    window = QWidget()
    layout = QVBoxLayout()

    def openFunc():
        # Opens a file from the system selector
        from tkinter import Tk     # from tkinter import Tk for Python 3.x
        from tkinter.filedialog import askopenfilename

        Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
        filename = askopenfilename() # show an "Open" dialog box and return the path to the selected file
        print(filename)

    openBtn = QPushButton("Open File")
    openBtn.clicked.connect(openFunc)
    layout.addWidget(openBtn)

    window.setLayout(layout)
    window.show()

    app.exec()