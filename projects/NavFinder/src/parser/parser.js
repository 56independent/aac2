const Path = require("path");
const prompt = require('prompt-sync')();
const fs = require("fs")
const fsPromises = require("fs")


/* 
Parser for NavFinder
*/

module.exports = {
    JSON: function() {

        var choice = {};
        var gameData = {};

        console.log("")

        if (fs.existsSync("./save.json")) {
            console.log("Hello! Would you like to restore a previous save? [y/n]");
            choice.restore = '';
            choice.restore = prompt('');

            if (choice.restore == "y") {
                text = fs.promises.readFile("./save.json");
                gameData = JSON.parse(json.stringify(text));

                console.log(gameData)
            } if (choice.restore == "n") {
                // Well, i guess we make a file. :-/
                choice.make = true;
            } else {
                console.log("Please make a correct choice next time");
            }
        } else {choice.make = true;}

        if (choice.make) {
            console.log("")
            console.log("No values are checked and this won't cause bugs. Do whatever. This is all just text anyways.")
            console.log("")

            console.log("How much hp? [1-100]");
            gameData.hp = prompt('');
            console.log("How many weapons?")
            gameData.weapons = prompt('');

            jsonData = JSON.stringify(gameData);

            console.log(jsonData);

            fs.promises.writeFile("save.json", jsonData);
        }
    }
}