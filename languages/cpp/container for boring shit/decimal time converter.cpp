/*
broken:
flotas not exactly working? (line 25-39)
the second if statement not completed
*/

# include <iostream>

using namespace std;

int main(){

    int choice;
    //tells you what options you have
    cout << "here is what you can choose:\n";
    cout << "   1 convert imperial to decimal time\n";
    cout << "   2 convert decimal into imperial time\n \n";
    cout << "what do you choose?\n";
    cin >> choice;

    if (choice == 1) {
        int hours;
        int minutes;

        float decmins;
        float dechours;
        float dectime;

        cout << "how many hours?\n";
        cin >> hours;
        cout << "how many minutes?\n";
        cin >> minutes;

        decmins = minutes/60;
        dechours = hours;

        dectime = decmins+dechours;

        cout << "your time was " << dectime << " in decimal units!\n";
    }
}
