# include <iostream>

using namespace std;

int main(){
    int focusNumber = 1;
    int terminator;
    int devideThree;
    int devideSeven;

    cout << "fizzbuzz program. what do you want the terminating number to be?\n";
    cin >> terminator;

    while (focusNumber != terminator){

        if (focusNumber%3 == 0){
            cout << "fizz ";
        } else if (focusNumber%7 == 0){
            cout << "buzz";
        } else {
            cout << focusNumber;
        };

        cout << "\n"; // there is a command to make newline, without a string, but this works!
        focusNumber++; //forgot to add this. had an infinite stream of 1s, had to restart XD
    };
}