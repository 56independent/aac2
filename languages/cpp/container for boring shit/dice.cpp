/*
dice program
so you can roll... DICE!
*/

# include <iostream>
# include <stdlib.h>
# include <unistd.h>

using namespace std;

int lowest;
int highest;
int generated;
string again;
string keep = "n";

int main(){
    cout << "so, you have decided to run the dice program. sobeit\n";
    sleep(0.5);
    randomizer:
        if (keep == "n"){
            cout << "what is your lowest possible number to roll?\n";
            cin >> lowest;
            cout << "what is the highest number possible to roll?\n";
            cin >> highest;
        }
        sleep(0.5);
        generated = rand() % highest + lowest;
        cout << "your number was " << generated << ", generated between " << lowest << " and " << highest << " .\n";
        cout << "do you wish to try again?(y/n)\n";
        cin >> again;
        if (again == "y"){
            cout << "do you wish to keep the same figures? (y/n)\n";
            cin >> keep;
            goto randomizer;
        if (again == "n"){
            cout << "bye. i never liked you anyways";
            return 0;
        }

    }

}
