1 - terminal

1.1 - all commands

B time - see what time and maybe date it is  
B run - run a program 
w help - get help on all functions  
w exit OR leave OR quit - get yerself out of my lawn 
w outputter - change outputter to what you like
w echoer - repeat what you just said
w fart - say fart however many times you like ;-)
w america - swear word warning
w france - french satire
b todo - run the todo list program
B pi - calculate pi to a user-defined amount of digits
w invalid - find why a scientists work is INVALID
w russianiser - turn some characters russian
b braintexteriser - turn from brainfuck into a .txt file, human readble
B brainpythoniser - turn from brainfuck into a runnable .py file
w fortuneTell - tell your fortune... *ghost noises*
w tileCalc - calculate tiles needed
b passGuess - guess a password. enter in a password, and see how many repeats it takes
w mainMenu - open main menu
w fib - fibbonachi sequence maker
B timer - time related functions
w square - generate square numbers
w x2 OR +x - times 2 by itself (or + itslef)
w morgatage - calculate morgatage
b change - calculate change in coins from certain amount of money
b sum - caculate the sum of all multiples of 5 and 3, to a certain amount
B csv - read and write csb files
B quiz - word game
b fibExtra - problem 2 of the euler project archive
b prime - calculate prime numbers, to a certain cap

1.2 - how they are broken

time - i dont know how to get the current time
run - i have no idea how to run a program
todo - havent yet learnt to read to files, and i cant read from them either.
pi - no idea of what equation to use to calculate pi
brainpythoniser - i am yet to invent a series of python commands for brainfuck. 
passGuess - alphanumeric password guessing not coded yet; i need to know how to add a string, like how you do int++.
timer - not coded just yet.
change - broke after adding banknotes.
sum - probably wrong.
csv - not coded yet.
quiz - see csv.
fibExtra - too laggy.
prime - not outputtin' anythin'

1.3 - notes for future me on this code, and how to add to it

!!READ SECTION 2 FIRST!!
ok, so first off, any long command definitons go inside their own private functions, underneat mainMenu. 
next, use double quotes, and dont add "using namespace std". sure, in the future you can crtl+h and remove the double 
quotes, but keep withthe style. 

2 - notes for contributors

dont contribute unless necessary. this is my private code, open source for you to look at, but i will probably reject
any changes and impliment my own. by all means, give me ideas, just dont change the code without asking.
this section is just for future me, to go "wow!". anyways, keep with the style of the code inside the programs. sure,
you may like apostrophies more then double quotes (traitor), but keep using double quotes. consistency is more important
then your prefrances. also, comment your # includes. it really helps when i know what.. er... library? i have to import when i 
want to do something. also, dont comment too much either, just explain the general gist of what the code does. like "//first, 
we declare varibales then recive input, then we do so and so, then we output stuff". commenting before/after every {}/{ is a
generally good rule of thumb. make sure that once you have coded it, ensure that i can see how it works, using just the 
functions alone. if not, just comment it.

