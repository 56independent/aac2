# include <iostream> //io (seriously, how do you not know what this does??)
# include <stdlib.h> //exit functions
# include <unistd.h> //sleep (remember, its usleep(1000000);)
# include <chrono> //date + time
# include <stdlib.h> //random numbers?!
# include <fstream> //file writing and stuff.

//help menu,that acts as an index of every command
void help(){ //command explanations down here:
    std::cout << "welcome to the help program. a w before the command means it works, a b means it does not completly work, B means it is completely broken. here all all commands available:" << std::endl ;
    std::cout << "B time - see what time and maybe date it is " << std::endl ;
    std::cout << "B run - run a program" << std::endl ;
    std::cout << "w help - get help on all functions " << std::endl ;
    std::cout << "w exit OR leave OR quit - get yerself out of my lawn" << std::endl ;
    std::cout << "w outputter - change outputter to what you like" << std::endl;
    std::cout << "w echoer - repeat what you just said" << std::endl;
    std::cout << "w fart - say fart however many times you like ;-)" << std::endl;
    std::cout << "w america - swear word warning" << std::endl;
    std::cout << "w france - french satire" << std::endl;
    std::cout << "b todo - run the todo list program" << std::endl;
    std::cout << "b pi - calculate pi to a user-defined amount of digits" << std::endl;
    std::cout << "w invalid - find why a scientists work is INVALID" << std::endl;
    std::cout << "w russianiser - turn some characters russian" << std::endl;
    std::cout << "b braintexteriser - turn from brainfuck into a .txt file, human readble" << std::endl;
    std::cout << "B brainpythoniser - turn from brainfuck into a runnable .py file" << std::endl;
    std::cout << "w fortuneTell - tell your fortune... *ghost noises*" << std::endl;
    std::cout << "w tileCalc - calculate tiles needed" << std::endl;
    std::cout << "b passGuess - guess a password. enter in a password, and see how many repeats it takes" << std::endl;
    std::cout << "w mainMenu - open main menu" << std::endl;
    std::cout << "B timer - time related functions" << std::endl;
    std::cout << "w fib - fibbonachi sequence maker" << std::endl;
    std::cout << "w square - generate square numbers" << std::endl;
    std::cout << "w x2 OR +x - times 2 by itself (or + itslef)- " << std::endl;
    std::cout << "w morgatage - calculate morgatage" << std::endl;
    std::cout << "b change - calculate change in coins from certain amount of money" << std::endl;
    std::cout << "b sum - caculate the sum of all multiples of 5 and 3, to a certain amount" << std::endl;
    std::cout << "B NickCsv - read and write csb files" << std::endl;
    std::cout << "B quiz - word game" << std::endl;
    std::cout << "b fibExtra - problem 2 of the euler project archive" << std::endl;
    std::cout << "b prime - calculate prime numbers, to a certain cap" << std::endl;
    std::cout << "b mesurment - variety of mesurment related functions" << std::endl;
    std::cout << "w sicktired - repeat \"i am sick and tired of you!\" many many times" << std::endl;
    std::cout << "w russian - switch language to russian" << std::endl;
    std::cout << "B twoSecret - enter a string of letters, then recive two streams of numbers. you can reverse this, and enter two numbers." << std::endl;
    //std::cout << "" << std::endl;
}

//i know, useless as of now. srry.
void mainmenu(){
    std::string choice;
    top:
        std::cout << "welcome to terminal. here are your choices:" << std::endl ;
        std::cout << "   1 - help menu, for more functions." << std::endl ;
        std::cout << "   2 - close" << std::endl ;
        std::cout << "   3 - rebuild skynet" << std::endl ;
        std::cout << "   4 - enter terminal" << std::endl ;
        std::cout << std::endl;

        std::cout << "enter the number of your choice here" << std::endl ;
        std::cin >> choice;
        if (choice == "1"){
            help();
        } else if (choice == "2"){
            std::cout << "bye, never like you anyways!" << std::endl ;
            exit(1);
        } else if (choice == "3"){
            std::cout << "establishing skynet..." << std::endl ;
            usleep(3000000);
            std::cout << "communicating with network" << std::endl ;
            usleep(3000000);
            std::cout << "starting machine learning process" << std::endl ;
            usleep(3000000);
            std::cout << "e: 2 TB memory needed. shutting down" << std::endl ;
            usleep(3000000);
            std::cout << "bye!" << std::endl ;
        } else if (choice == "4"){
        } else {
            std::cout << "sorry, \"" << choice << "\" is not a valid command." << std::endl;
            goto top;
        }
}

//commands here taken from main, and into their own private functions. just for easier maintinince.
void changeProgram(){
    double inputMoney;
            double moneyLeft;
            int amountb50;
            int amountb20;
            int amountb10;
            int amountb5;
            int amount2;
            int amount1;
            int amount50;
            int amount20;
            int amount10;
            int amount05;
            int amount02;
            int amount01;
            int amountShrodinger = 0;

            std::cout << "how much money do you need to give out in change? (in pounds, numbers only)" << std::endl;
            std::cin >> inputMoney;

            moneyLeft = inputMoney;

            while (moneyLeft != 0){
                if (moneyLeft >= 50){
                    moneyLeft -= 50;
                    amountb50 += 1;
                } else if (moneyLeft >= 20){
                    moneyLeft -= 20;
                    amountb20 += 1;
                } else if (moneyLeft >= 10){
                    moneyLeft -= 10;
                    amountb10 += 1;
                } else if (moneyLeft >= 5){
                    moneyLeft -=5;
                    amountb5 += 1;
                } else if (moneyLeft >= 2){
                    moneyLeft -= 2.00;
                    amount2 += 1;
                } else if (moneyLeft >= 1){
                    moneyLeft -= 1.00;
                    amount1 += 1;
                } else if (moneyLeft >= 0.50){
                    moneyLeft -= 0.50;
                    amount50 += 1;
                } else if (moneyLeft >= 0.20){
                    moneyLeft -=0.20;
                    amount20 += 1;
                } else if (moneyLeft >= 0.10){
                    moneyLeft -=0.10;
                    amount10 += 1;
                } else if (moneyLeft >= 0.05){
                    moneyLeft -= 0.05;
                    amount05 += 1;
                } else if (moneyLeft >= 0.02){
                    moneyLeft -= 0.02;
                    amount02 += 1;
                } else if (moneyLeft >= 0.01){
                    moneyLeft -= 0.01;
                    amount01 += 1;
                }
            }
            std::cout << "coins needed are: ";
            if (amountb50 > 0){
                std::cout << amountb50 << " £50 banknotes, ";
            } if (amountb20 > 0){
                std::cout << amountb20 << " £20  banknotes, ";
            } if (amountb10 > 0){
                std::cout << amountb10 << " £10 banknotes, ";
            } if (amountb5 > 0){
                std::cout << amountb5 << " £5 banknotes";
            } if (amount2 > 0 ) {
                std::cout << amount2 << " £2 coins, ";
            } if (amount1 > 0 ) {
                std::cout << amount1 << " £1 coins, ";
            } if (amount50 > 0) {
                std::cout << amount50 << " £0.50 coins, ";
            } if (amount20 > 0) {
                std::cout << amount20 << " £0.20 coins, ";
            } if (amount10 > 0) {
                std::cout << amount10 << " £0.10 coins, ";
            } if (amount05 > 0) {
                std::cout << amount05 << " £0.05 coins, ";
            } if (amount02 > 0) {
                std::cout << amount02 << " £0.02 coins, ";
            } if (amount01 > 0) {
                std::cout << amount01 << " £0.01 coins, ";
            } if (amountShrodinger > 0) {
                std::cout << amountShrodinger << " shrondinger coins (looks like this hasnt been coded properly.).";
            }
                std::cout << std::endl;
}
void russianiser(){
    std::string text;
    std::string temp;
    std::string output;
    int characterNumber = 0;
    int repeats;
    int randNum;

    std::cout << "enter text to convert to half-russian (place an @ at the ned of the text!)" << std::endl;
    std::getline(std::cin,text,'@');
    repeats = text.length(); // set repeats to the length of the std::string
    while (characterNumber != repeats){
        temp = text.at(characterNumber); // read the first character of the std::string
        if (temp == "N"){
            output += "И";
        } else if (temp == "A"){
            output += "А";
        } else if (temp == "B"){
            output += "В";
        } else if (temp == "E"){
            output += "Е";
        } else if (temp == "3"){
            output += "З";
        } else if (temp == "K"){
            output += "К";
        } else if (temp == "M"){
            output += "М";
        } else if (temp == "H"){
            output += "Н";
        } else if (temp == "O"){
            output += "О";
        } else if (temp == "P"){
            output += "Р";
        } else if (temp == "C"){
            output += "С";
        } else if (temp == "T"){
            output += "Т";
        } else if (temp == "Y"){
            output += "У";
        } else if (temp == "X"){
            output += "Х";
        } else if (temp == "W"){
            output += "Ш";
        } else if (temp == "R"){
            output += "Я";
        } else if (temp == "a"){
            output += "а";
        } else if (temp == "e"){
            output += "е";
        } else if (temp == "n"){
            output += "и";
        } else if (temp == "3"){
            output += "з";
        } else if (temp == "k"){
            output += "к";
        } else if (temp == "m"){
            output += "м";
        } else if (temp == "h"){
            output += "н";
        } else if (temp == "o"){
            output += "о";
        } else if (temp == "p"){
            output += "р";
        } else if (temp == "c"){
            output += "с";
        } else if (temp == "t"){
            output += "т";
        } else if (temp == "y"){
            output += "у";
        } else if (temp == "x"){
            output += "х";
        } else if (temp == "4"){
            output += "ч";
        } else if (temp == "w"){
            output += "ш";
        } else if (temp == "b"){
            //since there are two cryllic look-alikes for b, we choose a random one
            randNum = rand() % 1 + 2;
            if (randNum == 1){
                output += "Ь";
            } else if (randNum == 2){
                output += "в";
            }
        } else if (temp == "1"){
            std::string temp2;
            characterNumber++; //we increase it by one to get the character next to it.
            temp2 = text.at(characterNumber);
            if (temp2 == "0" && temp == "1"){ //if the character is 0, then its a "10", 
                output += "Ю";
            } else {
                characterNumber--;
                output += temp;
            }
        } else {
            output += temp;
        }
        characterNumber++;
    }
    std::cout << output << std::endl;
    //clipboard.add(output) //add std::string to clipboard
    std::cout << "copiy the output yourself, tramp (crtl+SHIFT+c shift is important for some reason)" << std::endl;
}
void brainfucktexteriser(){
    std::string brainfuck;
    std::string output;
    std::string filename;
    std::string temp;
    int charnumber = 0;
    int repeats;
    int cellNumber;

    std::cout << "what is the filename (place @ istead of .txt . ensure the @ is added)" << std::endl;
    std::getline(std::cin,filename,'@');
    filename += ".txt";
    std::cout << "enter your brainfuck string, and place an @ at the end." << std::endl;
    std::getline(std::cin,brainfuck,'@');

    repeats = brainfuck.length(); //set repeats to the amount of letters in std::string. 
    std::ofstream outfile;
    outfile.open(filename);

    while (repeats != charnumber){
        temp = brainfuck.at(charnumber); //read the first letter of the std::string, put it in temp
        charnumber++; 
        if (temp == "<"){
            output = "move reader to left ";
        } else if (temp == ">"){
            output = "move reader to the right ";
        } else if (temp == "+"){
            output = "increase value in current cell by one ";
        } else if (temp == "-"){
            output = "decrease value in current cell by one ";
        } else if (temp == "{"){
            output = "start ";
        } else if (temp == "}"){
            output = "goto start if current cell != 0 , otherwise continue ";
        } else{
            //nothin
        }
        outfile << output << std::endl;
        std::cout << output << std::endl; //also write it out in the terminal.
    }
    outfile.close();
}
void tileCalc(){
    int floorWidth;
    int floorLength;
    int tileWidth;
    int tileLength;
    double tilesNeededOnWidth;
    double tilesNeededOnLength;
    double tileCost;
    double totalCost;
    double tilesNeeded;

    std::cout << "what is the width of the tile, in cm?" << std::endl;
    std::cin >> tileWidth;
    std::cout << "what is the length of the tile in cm?" << std::endl;
    std::cin >> tileLength;
    std::cout << "what is the width of the floor, in cm?" << std::endl;
    std::cin >> floorWidth;
    std::cout << "what is the length of the floor, in cm?" << std::endl;
    std::cin >> floorLength;
    std::cout << "what is the cost of each tile? (pence)" << std::endl;
    std::cin >> tileCost;

    tilesNeededOnWidth = floorWidth/tileWidth ;
    tilesNeededOnLength = floorLength/tileLength;
    tilesNeeded = tilesNeededOnLength*tilesNeededOnWidth;
    totalCost = tilesNeeded*tileCost;
    totalCost = totalCost/100; //convert to pounds

    std::cout << "the amount of tiles needed are " << tilesNeeded << " tiles, at £" << totalCost << " total cost. "  << std::endl;

}
void passGuess(){
    std::string choice;
    int repeats;
    choose:
    std::cout << "would you like an (a)lphanumeric password or a (i)nteger one?";
    std::cin >> choice;
    std::cout << "enter a password to crack" << std::endl;
    if (choice == "i"){
        int password;
        int theGuess;

        std::cin >> password;
        while (theGuess != password){
            theGuess++;
            repeats++;
        }
        std::cout << "your password was " << password << ". the guessed password was " 
        << theGuess << ", which was guessed in " << repeats << " repeats." << std::endl;
    } else if (choice == "a"){
        std::string password;
        
        std::cin >> password;

        std::cout << "not coded yet" << std::endl;
        goto choose;
    }
}
void pi(){
    int length;
    std::string pi;

    std::cout << "how many digits to figure out?" << std::endl;
    std::cin >> length;

    while (pi.length() != length){
        //code for formulating pi goes here
    }
    std::cout << "pi is about " << pi << " but not quite.";
}
void sumOfAll(){ 
    int incrimenter = 1;
    int sumOf3;
    int sumOf5;
    int max;

    std::cout << "what is the maximum number?" << std::endl;
    std::cin >> max;

    while (sumOf5 + sumOf3 < max){
        sumOf3 += incrimenter*3;
        sumOf5 += incrimenter*5;
        incrimenter++;
    }
    while (sumOf3 + sumOf5 > max){
        sumOf3  /= 3; //extra space, so the lines kind of line up.
        sumOf5 /= 5;
    }
    std::cout << "sum of all multiples of 5 and 3 that are below "<< max << " is " << sumOf3+sumOf5 << "." << std::endl;
}
void fibExtra(){
    int numOne = 1;
    int numTwo = 1;
    int numThree;
    int added;
    while (numThree < 4*10^6){ //4 M, or 4*10^6, or 4000000. i used standard form, to use only 6 characters, and not 7 XD
        numThree = numOne+numTwo;
        numOne = numTwo;
        numTwo = numThree;
        // line = std::to_string(numThree);
        // character = line.length();
        // temp = line.at(character); //commented out poor code, can use a modulus. also, to find last digit, use var%10
        if (numThree % 2 == 0){
            added += numThree;
        }
    }
    std::cout << added << std::endl;
}
void NickCsv(){
    std::string writestream;
    int choice;

    std::cout << "welcome to the main menu" << std::endl;
    std::cout << "  1 - write" << std::endl;
    std::cout << "  2 - read" << std::endl;
    std::cout << "what is your choice, idiot?" << std::endl;

    if (choice == 1){
        std::string choice = "totes not end, promise";
        int charnum;
        
        std::cout << "enter your text to enter (NXTRW for next row, END for ending)" << std::endl;
        
        while (choice != "END"){
			std::cin >> choice;
			if (choice == "NXTRW"){
				std::cout << std::endl;
			} while (charnum != choice.length()){ // main loop
				
			} 
			
		}
    }
}
void mesurement(){

    int choice;

    while (choice != 5){
        std::cout << "welcome to the main menu" << std::endl;
        std::cout << "b  1 - convert metric-imperial" << std::endl;
        std::cout << "B  2 - convert imperial-metric" << std::endl;
        std::cout << "B  3 - convert imperial units to imperial units" << std::endl;
        std::cout << "B  4 - convert metric to more metric units" << std::endl;
        std::cout << "W  5 - leave. go away." << std::endl ;
        std::cout << std::endl;
        std::cout << "what is your choice, lowlife?" << std::endl;
        std::cin >> choice;

        if (choice == 1){
            std::cout << "what unit?" << std::endl;
            std::cout << "  1 - inches" << std::endl;
            std::cout << "  2 - feet" << std::endl;
            std::cout << "  3 - miles" << std::endl;
            std::cout << "  4 - imperial equivlent of ml" << std::endl;
            std::cout << "  5 - imperial equivlent of cl" << std::endl;
            std::cout << "  6 - imperial equivlent of l" << std::endl;
        } else if (choice == 2){

        } else if (choice == 3){

        } else if (choice == 4){

        } else if (choice == 5){
            std::cout << "never like you anyways. now i hate you. leaving  a program like that? not alright!" << std::endl;
        } else {
            std::cout << "i dont know what you chose, but thats is NOT a valid option. for that, i hate you" << std::endl;
        }
    }
}
/* // commented out due to you know, incomplete.
void twoSecret(){
	char choice;
	std::cout << "what mode would you like? [d]ecoder or [e]ncoder?" << std::endl;
	std::cin >> choice;
	
	if (choice == d){
		bool end;
		while (end != true){
			int first;
			int second;
			int key;
			char letter;
			
			std::cout << "do you wish to quit? (y/n)" << std::endl;
			std::cin >> first; //yes, i know, using a varible for no reason.
			if (letter == y){
			end = true;
			} else if (letter == n){
				std::cout << "enter the first number" << std::endl;
				std::cin >> first;
				std::cout << "enter the second number" << std::endl;
				std::cin >> second;
				
				key = first+second;
				
				if (key == 1){
					letter = a;
				} else if (key == 2){
					letter = b;
				} else if (key == 3){
					letter = c;
				} else if (key == 4){
					letter = d;
				} else if (key == 5){
					letter = e;
				} else if (key == 6){
					letter = f;
				} else if (key == 7){
					letter = g;
				} else if (key == 8){
					letter = h;
				} else if (key == 9){
					letter = i;
				} else if (key == 10){
					letter = j;
				} else if (key == 11){
					letter = k;
				} else if (key == 12){
					letter = l;
				} else if (key == 13){
					letter = m;
				} else if (key == 14){
					letter = n;
				} else if (key == 15){
					letter = o;
				} else if (key == 16){
					letter = p;
				} else if (key == 17){
					letter = q;
				} else if (key == 18){
					letter = r;
				} else if (key == 19){
					letter = s;
				} else if (key == 20){
					letter = t;
				} else if (key == 21){
					letter = u;
				} else if (key == 22){
					letter = v;
				} else if (key == 23){
					letter = w;
				} else if (key == 24){
					letter = x;
				} else if (key == 26){
					letter = y;
				} else if (key == 27){
					letter = z;
				}
			}
		}	
	} else if (choice == e) {{
		bool end;
		while (end != true){
			int first;
			int second;
			int key;
			char letter;
			
			std::cout << "do you wish to quit? (y/n)" << std::endl;
			std::cin >> first; //yes, i know, using a varible for no reason.
			if (letter == y){
			end = true;
			} else if (letter == n){
				std::cout << "enter a letter to encode" << std::endl;
				std::cin >> letter;
			}
		}
    }	
}
*/

int main(){
    std::string command;
    bool exit;
    //std::string outputter = "\033[32;1mfake@terminal-really-fake\033[37m:\033[34m~\033[37m$ \033[37;24;21m"; // yep this looks like linux...
    std::string outputter = "hostname@username:~$ ";

    help();

    //main command reciving loop
    while (true){
        std::cout << outputter;
        std::cin >> command;
        std::cout << std::endl;
        //command definitons down here:

        { //brace here, so i can hide the if array. (used in ide)

            if (command == "time"){
                std::cout << "not coded yet :-/" << std::endl ;
            } else if (command == "run"){
                std::cout << "sorry not coded yet :-/" << std::endl;
            } else if (command == "help"){
                help();
            } else if (command == "exit" || command == "leave" || command == "quit"){
                return(1);
            } else if (command == "outputter"){
                std::cout << "what do you want your outputter to be?  ";
                std::cin >> outputter;
                std::cout << std::endl;
            } else if (command == "echoer"){
                std::string victim;
                std::cout << outputter << "echo";
                std::cin >> victim;
                std::cout << std::endl << victim << std::endl;
            } else if (command == "fart"){
                //works by magic, do not touch
                int repeats;
                std::string line;
                std::cout << "how many times shall i say the forbidden word?" << std::endl;
                std::cin >> repeats;
                std::cout << "all in (o)ne line or (s)eperate lines?" << std::endl;
                std::cin >> line;
                std::cout << std::endl;
                while (repeats != 0){
                    std::cout << "fart ";
                    if (line == "s"){
                        std::cout << std::endl;
                    }
                    repeats--;
                }
                std::cout << std::endl; //you can touch after here now.
            } else if (command == "france"){
                std::cout << "in france it is illegal to name your pig napeoleon. here is a script of what may happen if you do." << std::endl;
                std::cout << "pig seller: je suis name pig swene appele napeoplen" << std::endl;
                std::cout << "policeman: je illegal! illegal! cest ilegal! you, my friend are going to le prision, or le 5K frank fine!" << std::endl; 
            } else if (command == "null"){
                std::cout << "you discovered this command. good job. did you read the code?" << std::endl;
            } else if (command == "america"){
                std::cout << "FUCK YEAH AMERICA, GUNS GUNS GUNS!" << std::endl;
                std::cout << "FUCK YOU, OUTSIDER, AMERICAAAAAAAAAA GUN GUN GUN! SHOOT ALL THE KIDS ON THE STREET!!!" << std::endl;
            } else if (command == "todo"){
                std::string spareVariable;

                startForTodo:
                std::cout << "welcome to the main menu. what do you wanna do? (tell us please)" << std::endl;
                std::cout << "    1 - add to list" << std::endl;
                std::cout << "    2 - remove from list" << std::endl;
                std::cout << "    3 - view all entries" << std::endl;
                std::cout << "what is your choice?" << std::endl;
                std::cin  >> spareVariable;
                if (spareVariable == "1"){
                    //place some code here
                } else if (spareVariable == "2"){
                    //some more code
                } else if (spareVariable == "3"){
                    //lol, some code
                } else {
                    std::cout << "ACTUALLY WRITE THE CORRECT  NUMBER, WE ARE GOING BACK TO THE START BECAUSE OF YOU!" << std::endl;
                    goto startForTodo;
                }
            } else if (command == "invalid"){
                std::cout << "scientist: when our work is takeon out of context, it becomes worthless" << std::endl;
                std::cout << "the media: A SCIENTISTS WORK IS WORTHLESS?! read to find out why! also, ready why kylie jenners blush is too red for the day" << std::endl;
            } else if (command == "pi"){
                pi();
            } else if (command == "russianiser"){
                russianiser();
            } else if (command == "braintexteriser"){
                brainfucktexteriser();
            } else if (command == "brainpythoniser" ){

            } else if (command == "fortuneTell"){
                //declare variable, ask, recive input generate random number, respond based on random number.
                int num;
                std::string uselessVariable ;

                std::cout << "what is yor question? we will come up with an answer!" << std::endl;
                std::getline(std::cin, uselessVariable);

                num = rand() % 1 + 5;
                if (num == 1){
                    std::cout << "definately" << std::endl;
                } else if (num == 2){
                    std::cout << "posotive mayber" << std::endl;
                } else if (num == 3){
                    std::cout << "maybe" << std::endl;
                } else if (num == 4){
                    std::cout << "negative maybe" << std::endl;
                } else if (num == 5){
                    std::cout << "no" << std::endl;
                }
            } else if (command == "tileCalc"){
                tileCalc();
            } else if (command == "passGuess"){
                passGuess();
            } else if (command == "mainMenu"){
                mainmenu();
            } else if (command == "timer"){

            } else if (command == "fib"){
                int numOne = 1;
                int numTwo = 1;
                int numThree;
                int repeats;
                std::string line;
                
                std::cout << "how many figures do you wish to generate?" << std::endl;
                std::cin >> repeats;
                std::cout << "for seperate lines, input s, for them all to be on the same line, literally any other character" << std::endl;
                std::cin >> line;

                while (repeats != 0){
                    numThree = numOne+numTwo;
                    numOne = numTwo;
                    numTwo = numThree;
                    std::cout << numThree << " ";
                    if (line == "s"){ 
                        std::cout << std::endl; 
                    }
                    repeats--;
                }
            } else if (command == "x2" || command == "+x"){
                int repeats;
                int num = 2;
                std::string line;

                std::cout << "how many times do you want to repeat?" << std::endl;
                std::cin >> repeats;
                std::cout << "(o)ne line or (s)eperate lines?" << std::endl;
                std::cin >> line;
                while (repeats != 0){
                    std::cout << num << " ";
                    num  = num*2;
                    repeats--;
                    if (line == "s"){
                        std::cout << std::endl;
                    }
                }
            } else if (command == "square") {
                int num = 2;
                int repeats;
                std::string line;

                std::cout << "how many repeats?" << std::endl;
                std::cin >> repeats;
                std::cout << "(o)ne line or (s)eperate lines?" << std::endl;
                std::cin >> line;
                while (repeats != 0){
                    std::cout << num << " ";
                    num  = num*num;
                    repeats--;
                    if (line == "s"){
                        std::cout << std::endl;
                    }
                }
            } else if (command == "morgatage") {
                int term;
                int price;
                double costOverTime;

                std::cout << "what is the term?" << std::endl;
                std::cin >> term;
                std::cout << "what is the price (pence) " << std::endl;
                std::cin >> price;

                costOverTime = price/term;
                costOverTime = costOverTime/100;

                std::cout << "£" << costOverTime << " per 1 term, over " << term << " terms and £" << price/100 << " total cost." << std::endl;
            } else if (command == "change"){
                changeProgram();
            } else if (command == "sum"){
                sumOfAll();
            } else if (command == "fibExtra"){
                fibExtra();
            } else if (command == "prime"){
                int focusNumber = 0;
                int devisor;
                int max;

                std::cout << "what is your maximiumm number?" << std::endl;
                std::cin >> max;

                bool failed;

                while (focusNumber < max){

                    failed = false;

                    focusNumber++;
                    devisor = focusNumber;
                    devisor--;

                    while (devisor != 1){
                        if (focusNumber % devisor != 0){ //until there isnt a remainder, it isnt prime.
                            devisor--;
                        } else if (focusNumber % devisor == 0){  
                            failed = true;
                        } 
                    }

                    if (failed != true){
                        std::cout << focusNumber << std::endl;
                        focusNumber++;
                    }

                }
            } else if (command == "NickCsv"){
                NickCsv();
            } else if (command == "mesurement"){
                mesurement();
            } else if (command == "sicktired"){
	        	std::string tiredAt;
	        	int repeats;
	        	std::cout << "how many times to repeat \"being sick and tired at..." << std::endl;
	        	std::cin >> repeats;
	        	std::cout << "what are you sick and tired at? (example: ...with your behaviour, or ...with my cat" << std::endl;
	        	std::cin >> tiredAt;
	        	std::cout << "i am absoloutely " << std::endl;
	        	while (repeats != 0){
	        		repeats--;
	        		std::cout << "sick and tired of being: " << std::endl;
				}
				std::cout << "sick and tired with " << tiredAt << " !";
			} else if (command == "russian"){
                std::cout << "пажалюста прастй, я ни знаис парюски.:-(" << std::endl;
            } else {
                std::cout << "command \"" << command << "\" is not a valid command. try running \"help\" to see all working commands" << std::endl;
            }
            
            std::cout << std::endl;
        }
    }
}
