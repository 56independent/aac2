# include <iostream>
# include <fstream>
# include <string>
# include <array>
# include <unordered_map>

std::pair<std::string, std::string> parse_entry(std::string line) {
	std::size_t index_first_space = line.find(' ');
	if (index_first_space == std::string::npos) return {}; // Malformed
	return std::make_pair(line.substr(0, index_first_space), line.substr(index_first_space + 1));
}


std::unordered_map<std::string, std::string> load_strings(unsigned choice) {
	std::array<std::string, 2> file_names = {"english.local", "icelandic.local"};

	if (choice >= file_names.size()) {
		std::cout << "error, invalid language choice." << std::endl;
		exit(1);
	}

	std::ifstream file;
	file.open(file_names[choice]);

	std::unordered_map<std::string, std::string> ret;

	if (file.is_open()) {
		std::string line;

		while(std::getline(file, line)) {
			std::pair<std::string, std::string> entry = parse_entry(line);
			ret[entry.first] = entry.second;
		}
		
		file.close();
	}

	return ret;
}

int main() {
	unsigned language_choice;
	std::cin >> language_choice;

	std::unordered_map<std::string, std::string> localized_strings = load_strings(language_choice);

	std::cout << localized_strings["hello_message"] << std::endl;
	std::cout << localized_strings["error_message"] << std::endl;
	std::cout << localized_strings["amazing"] << std::endl;
	
}
