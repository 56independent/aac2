#!/usr/bin/perl

use strict 'vars';
use feature 'say';
use feature 'state';
use Audio::NoiseGen qw(play sine square triangle);
use Term::ReadKey qw(ReadMode ReadLine);
 
Audio::NoiseGen::init() || die 'No access to sound hardware?';

print "what is the input?\n";

$string = <STDIN>;

my @chars = split("", $string);

my $char = 0;
my $length = @chars;

while($char != $length){
    print "$chars[$char]";
    $char = $char+1;
    sleep(1);
    play ( gen => amp ( amount => 3, gen => &'sine'( freq => setfreq(440) ) ) );
}