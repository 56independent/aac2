#!/usr/bin/perl

# This will print "Hello, World"
print "Hello, world!\n\n";

=begin comment
This is all part of multiline comment.
You can use as many lines as you like
These comments will be ignored by the 
compiler until the next =cut is encountered.
=cut

$a = 10;
$var = <<"EOF";
This is the syntax for here document and it will continue
until it encounters a EOF in the first line.
This is case of double quote so variable value will be 
interpolated. For example value of a = $a
EOF
print "$var\n";

$var = <<'EOF';
This is case of single quote so variable value will be 
interpolated. For example value of a = $a
EOF
print "$var\n";

@ages = (25, 30, 40);             
@names = ("John Paul", "Lisa", "Kumar");

print "$names[0] = $ages[0]\n";

$crazyThing = "wow";

if (crazyThing == "wow"){
    # this is just like using c/c++ if statements, i like. not like the bash shit!
    print "hey.... wow!\n";
}

open(DATA, ">file.txt") or die "Couldn't open file file.txt, $!"; #seems agressive - open data or DIE.



close(DATA) || die "Couldn't close file properly";
