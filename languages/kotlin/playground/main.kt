// this file is for testing and fun, like the rust version. but more kotliny.
package playground 

import kotlin.system.exitProcess

import playground.filePlay.*
import playground.fizzbuzz.*
import playground.author.*
import playground.gui.*
import playground.dice.*

fun main() {
    while (true) {
        println("welcome to the playground! here are your choices:")
        println("   1 - file play")
        println("   2 - gui")
        println("   3 - dice")
        println("   4 - fizzbuzz")
        println("   56 - author's signature")
        println("   d - enable debug")
        println("to quit, its q, e, exit, or quit.")

        val choice = readLine()

        var debug: Boolean
        if (choice == "d") {debug = true } else {debug = false}

        if (debug == true){
            println("$choice was the command!")
        }

        if (choice == "quit" || choice == "q" || choice == "exit" || choice == "e") {
            exitProcess(0)
        } else if (choice == "1") {
            filePlay()
        } else if (choice == "2") {
            gui()
        } else if (choice == "3") {
            dice()
        } else if (choice == "4") {
            authour()
        } else if (choice =="d") {
            // this statement here only for protection
        } else {
            println("sorry, you entered the wrong choice!")
        }
        // template: } else if (choice == "X") {
    }
}
