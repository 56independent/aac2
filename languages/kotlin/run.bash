#!/bin/bash

help () {
	echo "help for kotlin runner!"
    echo "example:"
    echo "  ../run.bash <input (has to be a kotlin file)> <output (jar file name)> <y/n for run afterwards> <y/n to remove compiled afterwards>"
    echo "no file extensions! it assumes them automagically!"
    exit
} 

if [ "$3" = "-h" ]; then
    help
fi
if [ "$2" = "-h" ]; then
    help
fi
if [ "$1" = "-h" ]; then
    help
fi # repetitive? yes. fucked up? yes. do i care? no. 

kotlinc $1.kt -include-runtime -d $2.jar

if [ "$3" = y ]; then
    java -jar $2.jar
fi

if [ "$4" = y ]; then
    rm $2.jar
fi
