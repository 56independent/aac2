const clear = require('clear');
const CLI = require("clui");

module.exports = {
    CLI: function() {
        clear; // Prevent visual bugs

        var Line          = CLI.Line,
        LineBuffer    = CLI.LineBuffer;

        var outputBuffer = new LineBuffer({
        x: 0,
        y: 0,
        width: 'console',
        height: 'console'
        });




        var text = "£20"
        var text2 = "burgers";
        line = new Line(outputBuffer)
            .column(text)
            .column(text2/*.toFixed(3),20*/)
            .fill()
            .store();

        var value = Math.floor(Math.random() * (50-0) + 0);
        var maxValue = 50;
        var guageWidth = 40;
        var dangerZone = 30;
        var suffix = 'Value: ';

        console.log(CLI.Gauge(value, maxValue, guageWidth, dangerZone, suffix  + value));

        // var countdown = new CLI.Spinner('Exiting in 10 seconds...  ', ['|', '/', '-', '\\']);
        // countdown.start();

        var reqsPerSec = [10,12,3,7,12,9,23,10,9,19,16,18,12,12];
        console.log(CLI.Sparkline(reqsPerSec, 'reqs/sec'));

        outputBuffer.output();

        console.log("\n\n\n\n"); // Quarantine selector from... this
    }
}