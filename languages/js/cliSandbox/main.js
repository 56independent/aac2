const chalk = require('chalk');
const clear = require('clear');
const prompt = require('prompt-sync')();

const dice = require("./dice.js");
const CLI = require("./CLI.js")
const JSON = require("./JSON.js")

// var r l = readline.createInterface({
//  input: process.stdin,
//  output: process.stdout
// }); 

clear();

// console.log(
//     chalk.green(
//       figlet.textSync('Sandbox', { horizontalLayout: 'full' })
//     )
// );


while (true) {
    console.log("Welcome to the sandbox! Make your choice:");
    console.log("   1 - Dice");
    console.log("   2 - CLI");
    console.log("   3 - JSON Read/Writing")

    console.log("   q - Quit");

    var choice = '';
    choice = prompt(''); //empty for a reason 

    if (choice == "1") {
        console.debug("Starting option #1");
        dice.dice();
    } else if (choice == "2") {
        clear;
        CLI.CLI();
    } else if (choice == "3") {
        JSON.JSON();
    } else if (choice.toUpperCase() == "Q" || choice.toUpperCase() =="E" || choice.toUpperCase() =="EXIT" || choice.toUpperCase() =="QUIT") { // Please suggest better choices to me.
        // throw 'Not an error - user exited'; // Go away stupid software
        break;
    } else { // I'm just trying to make it work.
        console.log(chalk.red("Wrong choice \n"));
    }
}

console.log(chalk.green("Exited"))