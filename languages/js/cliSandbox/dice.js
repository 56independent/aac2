const clear = require('clear');
const prompt = require('prompt-sync')();

clear();

module.exports = {
    dice: function() {
        while (true) {
            console.log("Hello! Please choose from this menu:");
            console.log("   1 - 6-sided dice");
            console.log("   2 - 12-sided dice");
            console.log("   3 - 10 numbers");
            console.log("   4 - Own numbers");
            console.log("   b - Main Menu");
            console.log("   q - Quit");

            var choice = '';
            choice = prompt(''); //empty for a reason 

            var highest;
            var lowest = 1;

            if (choice == "1") {
                highest = 6;
            } else if (choice == "2") {
                highest = 12;
            } else if (choice == "3") {
                highest = 10;
            } else if (choice == "4") {
                console.log("What is your highest number? [6]");
                highest = prompt('');
                if (highest == null) {
                    highest = 6;
                }

                console.log("What is your lowest number? [1]");
                lowest = prompt('');
                if (lowest == null) {
                    lowest = 1;
                }
            } else if (choice == "b") {
                return 0;
            } else if (choice == "q") {
                throw 'Not an error - user exited'; // Go away stupid software
            } else {
                console.log("You slected the wrong button. For convinience, we assume you selected option \"1\".")
                highest = 6;
            }

            console.log("You rolled " + Math.floor(Math.random() * (highest-lowest) + lowest) + "!");
            console.log(""); // An extra newline wouldn't ever hurt. 
        }
    }
};