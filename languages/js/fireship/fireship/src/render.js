const { desktopCapturer } = require("electron/common");

const videoElement = document.querySelector("video");

const startBtn = document.getElementById("startBtn")
const stopBtn = document.getElementById("stopBtn")
const videoSelectBtn = document.getElementById("videoSelectBtn")

const videoSelectBtn = document.getElementById('videoSelectBtn');
videoSelectBtn.onclick = getVideoSources;

const {desktopCapturer} = require("electron") 

// Get the available video sources
async function getVideoSources() {
    const inputSources = await desktopCapturer.getSources({
      types: ['window', 'screen']
    });
  
    const videoOptionsMenu = Menu.buildFromTemplate(
      inputSources.map(source => {
        return {
          label: source.name,
          click: () => selectSource(source)
        };
      })
    );
  
  
    videoOptionsMenu.popup();
}

