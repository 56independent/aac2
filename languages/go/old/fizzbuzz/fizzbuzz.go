package main

import( 
    "fmt"
)

func main(){
	var begin int
	var cap int
	var i int
	var fizz int
	var buzz int

	fmt.Println("beggining number?")
	fmt.Scanln(&begin)
	i = begin

	fmt.Println("cap number?")
	fmt.Scanln(&cap)

	fmt.Println("fizz number? (blank for 5)")
	fmt.Scanln(&fizz)
	if fizz == 0 {
		fizz = 5
	}

	fmt.Println("buzz number? (blank for 5)")
	fmt.Scanln(&buzz)
	if buzz == 0 {
		buzz = 5
	}

	for i != cap {
		if i%fizz == 0 && i%buzz == 0 {
			fmt.Println("FizzBuzz")
		} else if i%fizz == 0 {
			fmt.Println("Fizz")
		} else if i%buzz == 0 {
			fmt.Println("Buzz")
		} else {
			fmt.Println(i)
		}
		i++
	}
}
