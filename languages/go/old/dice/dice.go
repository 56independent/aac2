package main

import( 
    "fmt"
    "math/rand"
    "time"
)

func main() {
    var highest int
    var lowest int
    var mode string
    var exit bool

    for exit != true{
        rand.Seed(time.Now().UnixNano())

        fmt.Println("what mode would you like? (e)xit, (2) sided dice (coin), (6) sided dice, (12) sided dice, or (u)ser defined?")
        fmt.Scanln(&mode)

        if mode == "2" {
            fmt.Println(rand.Intn(2))
        } else if mode == "6" {
            fmt.Println(rand.Intn(6))
        } else if mode == "12" {
            fmt.Println(rand.Intn(12))
        } else if mode == "u" {
            
            fmt.Println("lowest number in the range?")
            fmt.Scanln(&lowest)
            fmt.Println("highest number in the range?")
            fmt.Scanln(&highest)

            fmt.Println(rand.Intn(highest - lowest + 1) + lowest)
        } else if mode == "e" {
            fmt.Println("goodbye >:-(")
            exit = true
        }
    }
}
