echo "name of your file (disregarding extension and path)?"
read dirname

# commented out; refer to line 18

# echo "would you like to fill the file with stuff that needs to be in most .go files? (y/n)"
# read fill

filename="${dirname}/${dirname}.go"

mkdir $dirname

cd ./$dirname

touch $filename
cd ..

# commented out, the fi statemnt keeps on returning errors

# if [[ $fill == "y" ]] 
# then
    echo "package main \n\nimport(\n    \"fmt\"\n)\n\nfunc main(){\n    \n}" >> ./$dirname/$filename;
# fi
