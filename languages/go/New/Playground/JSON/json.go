package json
 
import (
    "fmt"
    "encoding/json" // Import for encoding
    "os"
    "log"
)
 

type Address struct {
    Street string
    City string
}
 
type Person struct {
    Name string
    Address Address
}

func write() {
    p := Person{
        Name: "Sherlock Holmes",
        Address: Address{
            "22/b Baker street", 
            "London",
        },
    }

    str, err := json.MarshalIndent(p, "", "    ")
    if err != nil {
        fmt.Println(err)
    }
    fmt.Println(string(str))

    os.WriteFile("json.json", []byte(str), 0666)
}

func read() {
    content, err := os.ReadFile("./json.json")
    if err != nil {
        log.Fatal("Error when opening file: ", err)
    }
 
    // Now let's unmarshall the data into `payload`
    var payload Person
    err = json.Unmarshal(content, &payload)
    if err != nil {
        log.Fatal("Error during Unmarshal(): ", err)
    }
 
    // Let's print the unmarshalled data!
    log.Printf("Name: %s\n", payload.Name)
    log.Printf("Street: %s\n", payload.Address.Street)
    log.Printf("Status: %s\n", payload.Address.City)
}

func JsonPlay() {
    write()
    read()
}