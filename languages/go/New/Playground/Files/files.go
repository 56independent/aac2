package files

import (
	"fmt"
    "os"
	"github.com/liamg/tml"
)

// todo: Add proper logging here

func Files() {
	var choice string
	tml.Printf("Choose - (r)ead or (w)rite?")
	fmt.Scanln(&choice)

	if choice == "w" {
		var content string
		var filename string

		tml.Printf("Hey, what would you like to write?")
		fmt.Scanln(&content)

		tml.Printf("What is the filename?")
		fmt.Scanln(&filename)

		os.WriteFile(filename, []byte(content), 0666)
	} else if choice == "r" {
		var filename string
		tml.Printf("Filename?")
		fmt.Scanln(&filename)

		fmt.Println(os.ReadFile(filename))
	}
}