package main

import (
	b "index.go/billSplitter"
	f "index.go/Files"
	// "index.go/gui"
	j "index.go/JSON"
	l "index.go/log"
	fl "index.go/Flooder"
	rps "index.go/RPS" // Rock Paper Scissors	
	g "index.go/guess"
	c "index.go/calculator"
	
	"fmt"
	"os"
	"github.com/liamg/tml"
	"time"
)

func main(){
	for true {
		tml.Println("\n<green>Hey, choose one of the following options:</green>")
		fmt.Println("b	1 - Splitting Bills")
		fmt.Println("w	2 - Files")
		// fmt.Println("x	3 - GUI")
		fmt.Println("x	4 - JSON")
		fmt.Println("w	5 - log")
		fmt.Println("w	6 - File Flooder")
		fmt.Println("w	7 - Flooder Cleanup")
		fmt.Println("w	8 - guessnumber")
		fmt.Println("b	9 - rock paper scissors")
		fmt.Println("?	10 - Calculator")
		fmt.Println("w	e/q/quit/exit - exit")
		tml.Println("<green>Meanings: w = working, b = bug/incomplete, B = broken, X = completely borked, won't work.</green>")

		var choice string
		fmt.Scanln(&choice)

		if choice == "1" {
			b.Bills()
		} else if choice == "2" {
			f.Files()
		// } else if choice == "3"{
		// 	gui()	
		} else if choice == "4" {
			j.JsonPlay()
		} else if choice == "5" {
			l.LogProgram()
		} else if choice == "6" {
			fl.Flood()
		} else if choice == "7" {
			fl.CleanUp()
		} else if choice == "8" {
			g.Guess()
		} else if choice == "9" {
			rps.Rps()
		} else if choice == "10"{
			c.Calculator()
		} 
		else if choice == "e" || choice == "q" || choice == "exit" || choice == "quit" {
			os.Exit(0)
		} else {
			tml.Println("<red>Sorry, you did it WRONG.</red>")
			time.Sleep(2 * time.Second)
		}
	}
}