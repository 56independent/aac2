package guess

import (
    "fmt"
    "github.com/liamg/tml"
    "math/rand"
    "time"
    "strconv"
)

func Guess() {
    tml.Println("<green>Guessing game! Type e or q to exit!</green>")
    for {
        var top string
        var bottom string
        var tries string

        numbers:
            tml.Println("What is your highest number?")
            fmt.Scanln(&top)
            tml.Println("What is the lowest number?")
            fmt.Scanln(&bottom)
        top:
            tml.Println("How many tries do you want?")
            fmt.Scanln(&tries)

        if ( bottom == "q" || bottom == "e") || ( top == "q" || top == "e") || ( top == "q" || top == "e") {
            break
        }

        topint, err := strconv.Atoi(top)
        if err != nil {
            tml.Println("Please use a number on the highest number, and not letters.")
            tml.Println("")
            goto numbers
        }

        bottomint, err := strconv.Atoi(bottom)
        if err != nil {
            tml.Println("Please use a number on the lowest number, and not letters.")
            tml.Println("")
            goto numbers
        }
        
        triesint, err := strconv.Atoi(tries)
        if err != nil {
            tml.Println("Please use a number on tries, and not letters.")
            tml.Println("")
            goto numbers
        }

        var guess string

        rand.Seed(time.Now().UnixNano())
        chosen := (rand.Intn(topint - bottomint + 1) + bottomint)

        for i := 0; i < triesint; i++ {
            guess:
                tml.Println("What is your guess?")
                fmt.Scanln(&guess)

            if guess == "q" || guess == "e"{
                break
            }

            guessint, err := strconv.Atoi(guess)
            if err != nil {
                tml.Println("Please use a number on tries, and not letters.")
                tml.Println("")
                goto guess
            }

            if guessint > chosen {
                tml.Println("<red>Too high!</red>")
            } else if guessint < chosen {
                tml.Println("<red>Too low!</red>")
            } else if guessint == chosen {
                tml.Println("<green>Perfect!</green>")
                goto top
            }
        }

        if guess == "q" || guess == "e"{
            break
        }

        if i == triesint {
            tml.Println("Too many guesses!")
        }
    }
}