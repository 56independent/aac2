package billSplitter

// Bill Splitter program. It takes in user input of tax, percantage for each, does a check, and finishes.

// Abandoned due to confusion in for loops..

import ( 
    "fmt"
    "strconv"
)

func Bills() {
    // * Declares variables
    var tax string
    var payers string
    
    // * Main loop
    for true {
        fmt.Println("Welcome to the bill splitter program! Percentages are marked with \"%\", and should be a number. Currency units are marked with \"¤\" (The international currency sign), and can be a decimal. Normal numbers are marked with \"0\".")
        fmt.Println("")

        tax:
            fmt.Println("% - What is the tax?")
            fmt.Scanln(&tax)

            // * Boilerplate to convert to float64
            tax64, err := strconv.ParseFloat(tax, 64)
            if err != nil {
                fmt.Println("Please use a number, and not letters.")
                fmt.Println("")
                goto tax
            }
            fmt.Println(tax64) //debug

        
        payers:
            fmt.Println("How many payers do you have?")
            fmt.Scanln(&payers)

            // * Boilerplate to convert to int
            payersInt, err := strconv.Atoi(payers)
            if err != nil {
                fmt.Println("Please use a number, and not letters.")
                fmt.Println("")
                goto payers
            }
            fmt.Println(payersInt) //debug

        // taxCollector:
            for {

                fmt.Println("% - Tax for payer #" )
            }

    }
}