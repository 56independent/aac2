echo "name of assembly file (no .asm)?"
read name

$nameasm="${name}.asm"
$nameo="${name}.o"

nasm -f elf $nameasm

ld -m elf_i386 -s -o $name $nameo

./$name
