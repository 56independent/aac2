// java program for general use, idea stolen from a similar c++ program. made by 506independent

import java.util.Scanner;
import java.io.*;

class main {
	public static void main(String[] args) {
		main.guide();
		while (true) {
			System.out.println("\033[32;1mfake@terminal-really-fake\033[37m:\033[34m~\033[37m$ \033[37;24;21m");//sorry for the mess :-/
            Scanner input = new Scanner(System.in);
			String command = input.nextLine();

			if (command == "help"){
				main.guide();
			} else if (command == "fib"){
				fib();
			}

		}
	}			
	
	public static void guide() {										
		System.out.println("Welcome to utility.java. here are the top commands:");
		System.out.println("B fib - fibbonachi sequence generator, to either a certain amount of numbers or a cap number.");
		System.out.println("B tax - calculate \"grocery store tax\".");
		System.out.println("w help - show this");								
		System.out.println("key:");
		System.out.println("B - not coded");
		System.out.println("b - partially coded");
		System.out.println("w - has some features");
		System.out.println("W - completely works");
	}

	public static void  fib(){
		// for the fibbonachi sequence
		Scanner input = new Scanner(System.in);

		System.out.println("(n)umber cap or (c)ertain amount of numbers");
		String choice = input.nextLine();
		
		System.out.println("terminating number?");
		String iterations = input.next();

		int i = Integer.parseInt(iterations);
		int repeats = 1;

		int a = 1;
		int b = 1;
		int c = 1;

		while ((choice == "n" && a < i) || (choice == "c" && repeats != i)){
			a += b;
			System.out.println(a);
			b = a;
			a++;
			repeats++;
		}

	}


}
