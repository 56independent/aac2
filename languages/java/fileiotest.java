// java program to recive input and save it to a file. made by 506independent

import java.util.Scanner;
import java.io.*;

class Input {
	static String input(String question){
		System.out.println(question);		
		Scanner input = new Scanner(System.in);
		String name = input.nextLine();
		//scan.close();
		return name;
	}
	
	public static void main(String[] args)throws IOException {
		// essential variables. do not Scanner input = new Scanner(System.in);touch
		Scanner input = new Scanner(System.in);
		File output = new File("save.txt");
		output.createNewFile();
		FileWriter writer = new FileWriter(output);

		// yes i know, rather boilerplaty. but idc, this is just a 
		// small project, to see what i can do with mr java

		String firstName = Input.input("first name?");

		String lastName = Input.input("last name?");

		System.out.println("what is your age?");
		String age = input.next();
		
		System.out.println("what is your birthday? (dd/mm/yy)");
		String birthday = input.next();
		
		/*
		template to copypaste:
		
		String name = Input.input("");
		*/

		// to write to file
		writer.write("first name: " + firstName + "\nlast name: " + lastName + "\nage: " + age + "\nbirthday (dd/mm/yy): " + birthday); 
        writer.flush();
        writer.close();

		// i need to give credit to those that taught to me to write files: https://www.tutorialspoint.com/java/java_filewriter_class.htm

		// debug line
		// System.out.println("your name is " + firstName + " " + lastName);
	}
}

