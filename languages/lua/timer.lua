--[[
timer hown on display
ensure lcd or display is set to display, and time set to timer
]]
if event.type == "program" then
	timer = 1
elseif event.type == "digiline" then
	digiline_send("timer",1)
	timer = timer+1
	digiline_send("display", "time elasped is ".. timer)
