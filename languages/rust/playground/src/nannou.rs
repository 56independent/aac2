//use nannou::prelude::*;

use nannou::prelude::*;
// use nannou_osc as osc;

pub fn nannou() {
    nannou::app(model).update(update).run();
}

struct Model {
    _window: window::Id,
}

fn model(app: &App) -> Model {
    let _window = app.new_window().view(view).build().unwrap();

    Model { _window, /*sender*/ }
}

fn update(_app: &App, _model: &mut Model, _update: Update) {}

fn view(app: &App, _model: &Model, frame: Frame) {
        // Prepare to draw.
        let draw = app.draw();

        // Clear the background to purple.  
        draw.background().color(GREEN);
    
        let boundary = app.window_rect();
        let sine = app.time.sin();
        let slowersine = (app.time / 2.0).sin();
        let x = map_range(sine, -1.0, 1.0, boundary.left(), boundary.right());
        let y = map_range(slowersine, -1.0, 1.0, boundary.bottom(), boundary.top());
    
        // Draw a blue ellipse with default size and position.
        draw.ellipse().color(BLUE).x_y(x,y);
    
        let points = (0..50).map(|i| {
            let x = i as f32 - 25.0;          //subtract 25 to center the sine wave
            let point = pt2(x, x.sin()) * 20.0; //scale sine wave by 20.0
            (point, STEELBLUE)
          });
    
        draw.polyline()
        .weight(3.0)
        .points_colored(points)
        .x_y(x,y);
    
        // Write to the window frame.
        draw.to_frame(app, &frame).unwrap();
}