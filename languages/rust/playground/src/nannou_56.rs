// how is this broken? well... the dots arent arranged correctly :-/

use nannou::prelude::*;

pub fn nannou_56() {
    nannou::app(model).update(update).run();
}

struct Model {
    _window: window::Id,
}

fn model(app: &App) -> Model {
    let _window = app.new_window().view(view).build().unwrap();
    Model { _window }
}

fn update(_app: &App, _model: &mut Model, _update: Update) {}

fn view(app: &App, _model: &Model, frame: Frame) {
    let draw = app.draw();
    draw.background().color(BLACK);
    
    let mut x: f32;
    let mut y: f32;

    x = 1.0f32;
    y = 2.0f32;

    let size_factor = 25.0f32;
    let rect_colour = GREEN;
     
    { //to be able to collapse repetitive code.
    draw.rect()
        .color(rect_colour)
        .w(20.0)
        .h(20.0)
        .x_y(x*size_factor,y*size_factor);

    x = -5.0f32;
    y = 2.0f32;
     draw.rect()
        .color(rect_colour)
        .w(20.0)
        .h(20.0)
        .x_y(x*size_factor,y*size_factor);

    x = -5.0f32;
    y = 0.0f32;
     draw.rect()
        .color(rect_colour)
        .w(20.0)
        .h(20.0)
        .x_y(x*size_factor,y*size_factor);

    x = -1.0f32;
    y = 0.0f32;
    draw.rect()
        .color(rect_colour)
        .w(20.0)
        .h(20.0)
        .x_y(x*size_factor,y*size_factor);

    x = -1.0f32;
    y = -2.0f32;
    draw.rect()
        .color(rect_colour)
        .w(20.0)
        .h(20.0)
        .x_y(x*size_factor,y*size_factor);

     x = -5.0f32;
     y = -2.0f32;
    draw.rect()
        .color(rect_colour)
        .w(20.0)
        .h(20.0)
        .x_y(x*size_factor,y*size_factor);

    x = 1.0f32;
    y = 0.0f32; 
    draw.rect()
        .color(rect_colour)
        .w(20.0)
        .h(20.0)
        .x_y(x*size_factor,y*size_factor);

    x = 2.0f32;
    y = 0.0f32;
    draw.rect()
        .color(rect_colour)
        .w(20.0)
        .h(20.0)
        .x_y(x*size_factor,y*size_factor);

    x = 2.0f32;
    y = -2.0f32;
    draw.rect()
        .color(rect_colour)
        .w(20.0)
        .h(20.0)
        .x_y(x*size_factor,y*size_factor);

    x = 1.0f32;
    y = -2.0f32;
    draw.rect()
        .color(rect_colour)
        .w(20.0)
        .h(20.0)
        .x_y(x*size_factor,y*size_factor);

    x = 1.0f32;
    y = 2.0f32;
    draw.rect()
        .color(rect_colour)
        .w(20.0)
        .h(20.0)
        .x_y(x*size_factor,y*size_factor);

    x = 2.0f32;
    y = 2.0f32;
    draw.rect()
        .color(rect_colour)
        .w(20.0)
        .h(20.0)
        .x_y(x*size_factor,y*size_factor);
    }
    
    draw.to_frame(app, &frame).unwrap();
}
