// this entire program was designed as a test :-/
mod notify;
mod nannou;
mod gtk;
mod nannou_clock;
mod nannou_56;
mod tui;
mod struct1;
mod conf;

use std::thread::sleep;
use std::time::Duration;
use std::io;
use std::process;

fn main() {
    loop {
        print!("what do you want to test?\n
                q - quit\n
                1 - notifications \n
                2 - nannou \n
                3 - nannou, but making 56 \n
                4 - gui test \n
                5 - clock, for clocks \n
                6 - tui, for terminal text \n");

        let mut line = String::new();
        io::stdin()
            .read_line(&mut line)
            .expect("Failed to read line");

        let line = line.trim();
        
        println!("DEBUG - input was \"{}\"",line);

        if line == "1"{
            notif_test();
        } else if line == "2"{
            nannou::nannou();
        } else if line == "3"{
            nannou_56::nannou_56();
        } else if line == "4"{
            gtk::gtk();
        } else if line == "5"{
            nannou_clock::nannou_clock();
        } else if line == "6"{
            tui::tui(); 
        } else if line == "7"{
            struct1::struct1();
        } else if line == "8"{
            conf::conf();
        } else if line == "9"{
            spinny_thing();
        } else if line == "q"{
            process::exit(1);
        }
    }
}

fn notif_test(){
    notify::notify("test", "just testing intresting library", "firefox");
    let time = Duration::new(5, 0);
    sleep(time);
    notify::notify("poo", "OMG! HE EATS POO!", "firefox");
}

fn spinny_thing(){
    // wait till i have tui workin.
}