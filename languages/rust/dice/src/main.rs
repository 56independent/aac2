use std::string::String;
use std::io;
use rand::Rng;

fn main(){
    println!("dice program!");
    let mut minimum = String::new();
    let mut maximum = String::new();
    let mut keep_settings = String::new();
    let mut go_away_errors_please = String::new(); 

    keep_settings = "n".to_string();

    loop {
        if keep_settings == "n"{ // i know, this is shit for ux. but i have tried. this is the only thing that works.
            println!("what is the minimumm?");
            io::stdin()
                .read_line(&mut minimum)
                .expect("Failed to read line");
            
            println!("what is the maximumm?");
            io::stdin()
                .read_line(&mut maximum)
                .expect("Failed to read line");

            println!("keep settings?");
            io::stdin()
            .read_line(&mut keep_settings)
            .expect("Failed to read line");

        } else {
            println!("hit enter 2 continue!");
            io::stdin()
                .read_line(&mut go_away_errors_please)
                .expect("Failed to read line");
        }

        if minimum > maximum { //edgecase
            // let min = maximum;
            maximum = minimum;
            minimum = maximum; 
        }

        let minimum: u32 = match minimum.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };
        
        let maximum: u32 = match maximum.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };
        
        let random_number = rand::thread_rng().gen_range(minimum,maximum);

        println!("the dice picked {}", random_number);
    }
}

