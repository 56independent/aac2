#include <stdio.h>
#include <stdlib.h>

int main(){
    char str1[500];
    char str2[500];

    printf("Please enter a number. ");
    scanf("%s", str1);

    printf("\nPlease enter a second number. ");
    scanf("%s", str2);

    printf("\nYou entered: %s and %s", str1, str2);

    float fl1 = atof(str1);
    float fl2 = atof(str2);

    printf("\nAs floats, these are %f and %f", fl1, fl2);

    float added = fl1+fl2;

    printf("\nAnd added together, these are %f.", added);

    return 0;
}