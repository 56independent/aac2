# Introduction
This project was designed to test the interconnectivity of three very different programming languages.

To run, execute `make run`, which runs the program and allows the user to use the coal UI. `make compile` should be executed beforehand

# Compiling
Here are the commands, prefaced with `make `. 

* c - Compiles the c file
* python - Compiles the python programs
* compile - Runs `c` and `python`.
* run - Run the program
* all - Runs `compile` and `run`.
* clean - Removes all compiled files and automatically generated trash. Must be used before commiting. 
