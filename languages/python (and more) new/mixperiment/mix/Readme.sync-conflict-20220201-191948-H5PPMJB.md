# Introduction
This project was designed to test the interconnectivity of three very different programming languages.

# Compiling
Here are the commands, prefaced with `make `. The one users will need is `make run`

* c - Compiles the c file
* python - Compiles the python programs
* compile - Runs `c` and `python`.
* run - Run the program
* all - Runs `compile` and `run`.
* clean - Removes all compiled files and automatically generated trash
