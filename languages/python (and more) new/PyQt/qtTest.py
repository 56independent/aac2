from PyQt5.QtWidgets import *
app = QApplication([])
window = QWidget()
layout = QVBoxLayout()

layout.addWidget(QLabel("Label of Object"))
layout.addWidget(QPushButton("Object of which the Label describes"))

layout.addWidget(QLabel("Second Label"))
layout.addWidget(QPushButton("Second Object"))

window.setLayout(layout)
window.show()

app.exec()