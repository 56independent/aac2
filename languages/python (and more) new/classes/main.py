class myClass:
    def __init__(self, name, amountOfTrains, cost, points, quote):
        self.name = name
        self.amountOfTrains = amountOfTrains
        self.cost = cost
        self.points = points
        self.quote = quote
    
    def info(self):
        print("Hi! I am " + self.name + " and i own " + str(self.amountOfTrains) + " which alltogether cost " + str(self.cost) + ". This has earnt me " + str(self.points) + " points!")

    
me = myClass("56i", 0, 200, 30, "Heho!")
manager = myClass("Robert trainowner", 100, 5e4, 50, "Bah humbug!")

me.info()
manager.info()