'''
The timetable works on chance. Each subject is given a number, and this number is picked randomly:

|! Subject |! Number |
| Psychology | 1 |
| Physics | 2 |
| English | 3 |
| English Literature | 4 |
| Maths | 5 |
| Drama | 6 |
| Film Studies | 7 |
| Biology | 8 |
| Chemistry | 9 |

This is then put in a day-by-day timetable, only on weekdays:

|! Slot |! Activity |
| 1 | Dice |
| 2 | Dice |
| 3 | Hobby |
| 4 | Dice |
| 5 | Dice |

Each slot is of around 20 minutes. The hobby slot is composed of free time, in which, such stuff as these can be done, but is not limited to:

* Composing
* Programming
* Sysadmin tasks

Between each slot is a 5 minute break. Overall, these add up to `(20+5)*5`, or 125 minutes (2.08 hours)
'''
import random

def dotime():
    from datetime import datetime
    import time

    now = datetime.now()

    twentyMinutes = 60*20 # Length of slot
    twentyFiveMinutes = 60*25 # Total time of slot,factoring break. 

    timestamp = time.time()
    now = datetime.fromtimestamp(timestamp)

    dotime = "Slot 1 - " + str(now)

    dotime += " to " + str(datetime.fromtimestamp(timestamp+twentyMinutes)) + " Slot 2 - "
    
    return dotime


choice = input("[t]imetable or [s]ingleton? ")

subjects = ["Psychology", "Physics", "English", "English Literature", "Maths", "Drama", "Film Studies", "Biology", "Chemistry"]

if choice == "t":
    while True:
        subjectsChosen = []

        num = 0

        num = random.randrange(0, len(subjects))        
        subjectsChosen.append(subjects[num])

        for i in range(3):
            while subjects[num] in subjectsChosen:
                num = random.randrange(0,8)        
            subjectsChosen.append(subjects[num])

        subjectsChosen.insert(2, "Free Space")

        #timing = dotime()
        #print(timing)
        
        yn = input("Your timetable is " + str(subjectsChosen) + ". Ok? (y/n) ")

        if yn == "y":
            break
elif choice == "s":
    while true:
        num = random.randrange(0, len(subjects))        
        yn = input("Is " + subjects[num] + " ok? (y/n)")

        if yn == "y":
            break