import GUI
import cFunc

def globs():
    global gen1off
    global gen2off
    global gen3off
    global gen4off

    gen1off = False
    gen2off = False
    gen3off = False
    gen4off = False

def main():
    globs()
    cFunc.initalise()
    GUI.run()

main()