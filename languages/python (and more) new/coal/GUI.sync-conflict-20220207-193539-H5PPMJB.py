import tkinter as tk
import random
import re
import time

# coding=UTF-8

# WARNING: This program is full of repetititititition. This code is known to the state of california to cause eye cancer.

# Guide to code names:
## take g1ca. It is formed of: 
### <Generator address (gx)>
### <type of info, from steam (s), power (p), or cooling water (c)>
### <Whether it is the label for it or the value (a)>
# For steam

def run():
    # Functions
    def updateStats():
        # 350-650 Mw of power
        # 8.5e4-1e5 per minute tons per hour of steam
        # 540-600 degrees steam
        # 2.5e6-3.5e6 per minute cubic metres of cooling water 

        # Logic:
        ## Read values for each generator
        ## Change it by +/- 10-1% keeping within bounds
        ## ~~Disable button and save time~~
        ## ~~After 10 seconds, allow button to be reused.~~
        
        # Regex finding
        def update(label, min, max, prefix):
            d = re.compile("([\d\.]*)") # Proof: regexr.com/6eg20
            value = float(d.match(label["text"]).group(0))

            # Random change 
            done = False
            while done != True:
                percent = random.randint(-10,10)/100
                newValue = value+(value*percent)
                if newValue <= max:
                    done = True
                elif newValue >= min:
                    done = True
                else:
                    done = False

            newValueInt = int(newValue)
            label["text"] = f"{newValueInt} {prefix}"

        update(g1pa, 350, 650, "Mw")
        update(g2pa, 350, 650, "Mw")
        update(g3pa, 350, 650, "Mw")
        update(g4pa, 350, 650, "Mw")

        update(g1sa1, 540, 600, "℃")
        update(g2sa1, 540, 600, "℃")
        update(g3sa1, 540, 600, "℃")
        update(g4sa1, 540, 600, "℃")

        update(g1sa2, 8.5e4, 1e5, "m3/min")
        update(g2sa2, 8.5e4, 1e5, "m3/min")
        update(g3sa2, 8.5e4, 1e5, "m3/min")
        update(g4sa2, 8.5e4, 1e5, "m3/min")

        update(g1ca, 2.5e6, 3.5e6, "m3/min")
        update(g2ca, 2.5e6, 3.5e6, "m3/min")
        update(g3ca, 2.5e6, 3.5e6, "m3/min")
        update(g4ca, 2.5e6, 3.5e6, "m3/min")

        #updateBtn["state"] = "disabled"
        #updateBtn.bind("<Button-1>", check)
        #global now 
        #now = time.time()

    #def check(x):
    #    if now+3 < time.time():
    #        updateBtn["state"] = "normal"
    #        time.sleep(0.5)
    #        updateBtn.bind("<Button-1>", updateStats)

    def maintain():
        pass

    def shtDwn1():
        pass

    def shtDwn2():
        pass

    def shtDwn3():
        pass

    def shtDwn4():
        pass

    def shtDwnAll():
        pass

    # Summon labels
    ## Generator labels
    g1 = tk.Label(
        text="Generator 1"
        #columnspawn=2
    )
    
    g2 = tk.Label(
        text="Generator 2"
        #columnspawn=2
    )

    g3 = tk.Label(
        text="Generator 3"
        #columnspawn=2
    )

    g4 = tk.Label(
        text="Generator 4"
        #columnspawn=2
    )

    tot = tk.Label(
        text="Totals"
    )

    ## Powers
    g1p = tk.Label(
        text="Power"
    )

    g2p = tk.Label(
        text="Power"
    )

    g3p = tk.Label(
        text="Power"
    )

    g4p = tk.Label(
        text="Power"
    )

    tp = tk.Label(
        text="Power"
    )

    ## Steam
    g1s = tk.Label(
        text="Steam"
    )

    g2s = tk.Label(
        text="Steam"
    )

    g3s = tk.Label(
        text="Steam"
    )

    g4s = tk.Label(
        text="Steam"
    )

    ts = tk.Label(
        text="Steam"
    )
    ## Cooling Water
    g1c = tk.Label(
        text="Cooling Water"
    )

    g2c = tk.Label(
        text="Cooling Water"
    )

    g3c = tk.Label(
        text="Cooling Water"
    )

    g4c = tk.Label(
        text="Cooling Water"
    )
    
    tc = tk.Label(
        text="Cooling Water"
    )

    # Amounts.
    ## Powers
    g1pa = tk.Label(
        text="400 Mw"
    )

    g2pa = tk.Label(
        text="400 Mw"
    )

    g3pa = tk.Label(
        text="400 Mw"
    )

    g4pa = tk.Label(
        text="400 Mw"
    )

    tpa = tk.Label(
        text="1,600 Mw"
    )
    
    ## Steam
    g1sa1 = tk.Label(
        text="590 ℃"
    )

    g2sa1 = tk.Label(
        text="590 ℃"
    )

    g3sa1 = tk.Label(
        text="590 ℃"
    )

    g4sa1 = tk.Label(
        text="590 ℃"
    )

    tsa1 = tk.Label(
        text="590 ℃"
    )

    g1sa2 = tk.Label(
        text="9000 m3/min"
    )

    g2sa2 = tk.Label(
        text="9000 m3/min"
    )

    g3sa2 = tk.Label(
        text="9000 m3/min"
    )

    g4sa2 = tk.Label(
        text="9000 m3/min"
    )

    tsa2 = tk.Label(
        text="36000 m3/min"
    )

    ## Cooling Water
    g1ca = tk.Label(
        text="3,000,000 m3/min"
    )

    g2ca = tk.Label(
        text="3,000,000 m3/min"
    )

    g3ca = tk.Label(
        text="3,000,000 m3/min"
    )

    g4ca = tk.Label(
        text="3,000,000 m3/min"
    )

    tca = tk.Label(
        text="Cooling Water"
    )

    # Summon Buttons
    shtDwn1 = tk.Button(
        text="Shut Down",
        command=shtDwn1        
    )

    shtDwn2 = tk.Button(
        text="Shut Down",
        command=shtDwn2
    )

    shtDwn3 = tk.Button(
        text="Shut Down",
        command=shtDwn3
    )

    shtDwn4 = tk.Button(
        text="Shut Down",
        command=shtDwn4
    )

    shtDwnAll = tk.Button(
        text="Shut Down All",
        command=shtDwnAll
    )

    updateBtn = tk.Button(
        text="Update",
        command=updateStats
    )

    maintain = tk.Button(
        text="Maintenance",
        command=maintain
    )

    exitBtn = tk.Button(
        text="Exit",
        command=exit
    )

    # UI backend
    window = tk.Tk()
    #window.title("Coal Power Plant Control System (CPPCS)")

    # Emplace
    ## Generator Labels
    g1.grid(column=1, row=1)
    g2.grid(column=3, row=1)
    g3.grid(column=5, row=1)
    g4.grid(column=7, row=1)
    tot.grid(column=9, row=1)
    
    ## Power labels
    g1p.grid(column=1, row=2)
    g2p.grid(column=3, row=2)
    g3p.grid(column=5, row=2)
    g4p.grid(column=7, row=2)
    tp.grid(column=9, row=2)
    
    ## Steam Labels
    g1s.grid(column=1, row=3)
    g2s.grid(column=3, row=3)
    g3s.grid(column=5, row=3)
    g4s.grid(column=7, row=3)
    ts.grid(column=9, row=3)
    
    ## Cooling Water
    g1c.grid(column=1, row=5)
    g2c.grid(column=3, row=5)
    g3c.grid(column=5, row=5)
    g4c.grid(column=7, row=5)
    tc.grid(column=9, row=5)

    # Amounts
    ## Power labels
    g1pa.grid(column=2, row=2)
    g2pa.grid(column=4, row=2)
    g3pa.grid(column=6, row=2)
    g4pa.grid(column=8, row=2)
    tpa.grid(column=10, row=2)
    
    ## Steam Labels
    g2sa1.grid(column=4, row=3)
    g1sa1.grid(column=2, row=3)
    g3sa1.grid(column=6, row=3)
    g4sa1.grid(column=8, row=3)
    tsa1.grid(column=10, row=3)

    g2sa2.grid(column=4, row=4)
    g1sa2.grid(column=2, row=4)
    g3sa2.grid(column=6, row=4)
    g4sa2.grid(column=8, row=4)
    tsa2.grid(column=10, row=4)

    ## Cooling Water
    g1ca.grid(column=2, row=5)
    g2ca.grid(column=4, row=5)
    g3ca.grid(column=6, row=5)
    g4ca.grid(column=8, row=5)
    tca.grid(column=10, row=5)

    # End buttons
    shtDwn1.grid(column=1, row=6)
    shtDwn2.grid(column=3, row=6)
    shtDwn3.grid(column=5, row=6)
    shtDwn4.grid(column=7, row=6)
    shtDwnAll.grid(column=9, row=6)
    maintain.grid(column=1, row=7)
    exitBtn.grid(column=9, row=7)
    updateBtn.grid(column=1, row=8)

    # Embark!
    window.mainloop()
