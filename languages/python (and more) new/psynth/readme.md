# Abstract
This program emulates protein synthesis by passing files between programs and making 

# Usage
* In the main directory, run `python3 main.py`. 
* It will ask for the chromosome and gene numbers you need. 
* Once you have input the address, you will wait until the amino acids are found.

# Internal workings