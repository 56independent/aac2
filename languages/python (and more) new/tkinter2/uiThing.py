import tkinter as tk
import tkinter.messagebox
import logging

import phone
import coal
#import loggingBP


def ui():
    # Logging boilerplate
    #loggingBP.boilerplate()
    
    logger = logging.getLogger('simple_example')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('spam.log')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)

    # Button Functions
    def error():  # Bring up an error screen.
        def fix():  # Pretend to fix the "error"
            tk.messagebox.showinfo("Fixed!", "Your problem was fixed!")

        def dont_fix():  # Quit and stop
            tk.messagebox.showerror("Broken Forever",
                                    "Your problem is now eternally broken and nobody can fix this. Sorry.")
            exit()

        logger.error("Error occurred!")

        tk.messagebox.showerror("Error", "An error occurred and we want to cry now.")
        response = tk.messagebox.askyesno("Question", "Do you want to fix this?")

        #logger.debug(f"Response? Ah, {response}")

        if response:
            logger.info("Fixing the error!")
            fix()
        elif not response:
            logger.warning("... Not fixing the error?!")
            dont_fix()

    def call():  # Bring up a cellphone GUI
        phone.call_phone()

    def flash():  # Flashes the call button and enables it.
        call.flash()
        call["state"] = "normal"
        call.bind("<Button-1>", call)

    def coal():  # Start a coal power plant GUI
        coal.UI()

    def quit_personal():
        exit()

    logger.debug("Defined functions!")

    # UI backend
    window = tk.Tk()

    window.title("Test")

    logger.debug("Setup the backend!")

    # UI elements
    error = tk.Button(
        text="Cause an error",
        command=error
    )

    nothing = tk.Button(
        text="Do nothing whatsoever",
        command=flash
    )

    call = tk.Button(
        text="Call someone",
        command=call
    )

    coal = tk.Button(
        text="Run a coal power plant",
        command=coal
    )

    quit_personal = tk.Button(
        text="Quit",
        command=quit_personal
    )

    logger.debug("Summoned objects!")

    # Post
    call.unbind("<Button-1>")
    call["state"] = "disabled"

    # Placing UI
    error.grid(row=1, column=1)
    nothing.grid(row=1, column=2)
    call.grid(row=2, column=1)
    coal.grid(row=2, column=2)
    quit_personal.grid(row=3, column=1)

    logger.debug("Placed objects!")

    # call.state
    logger.info("Started Tkinter GUI")
    window.mainloop()
