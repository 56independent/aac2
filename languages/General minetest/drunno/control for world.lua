--[[
a control panel
port c is for the mesecon line for the forcefield,
port a is for lag tester (quicker light flashes, less lag)
to make it work, ensure the control panel is set to "paneL".
broken: no output ocurrs whenever i press a button.
]]
event.msg = "none"
digiline_send("panel","up to enable/disable forcefield, down to test lag (quicker light flashes, less lag")
if event.msg ~= "none" then
	if event.msg == "up" then
		if pin.c == true then
			port.c = false
			digiline_send("panel","orcefields disabled")
		elseif pin.c == false then
			port.c = true
			digiline_send("panel","forcefields enabled")
		end
	end
	if event.msg == "down" then
		if pin.a == true then
			port.a = false
			digiline_send("panel","lag tester disabled")
		elseif pin.a == false then
			port.a = true
			digiline_send("panel","lag tester enabled")
		end
	end
	interrupt(10)
end

