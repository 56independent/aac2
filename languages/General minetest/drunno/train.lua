--[[
    this is a program to:
    1 - recive a digilines signal
    2 - save the message to a variable of the same name as the channel
    3 - once recived the messages, send off to ze train info thing
]]

if event.type == "digiline" then
    if event.channel == "arrive" then
        mem.arrive = event.msg 
    elseif event.channel == "route" then
        mem.route = event.msg
    elseif event.channel == "depart" then
        mem.depart = event.msg
    elseif event.channel == "type" then
        mem.type = event.msg
    end

    digiline_send("info" , 
        mem.arrive ..
        "|" .. mem.route ..
        "|" .. mem.depart .. 
        "|" .. mem.type ) -- i hope it vorks
    
end
