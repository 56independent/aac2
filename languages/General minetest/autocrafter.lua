--[[ smartshops
table1 = { --for wood for chests
	{"default:tree","",""},
	{"","",""},
	{"","",""}
}

table2 = {
	{"default:wood","",""},
	{"","",""},
	{"","",""}
}

table3 = { --signs
	{"default:wood","default:wood","default:wood"},
	{"default:wood","default:wood","default:wood"},
	{"","default:stick",""}
}

table4 = { --locked chests (iron is quite hard to get by in my base tho, so i use gold and chests to make them by hand)
	{"default:wood","default:wood","default:wood"},
	{"default:wood","","default:wood"},
	{"default:wood","default:wood","default:wood"}
}

table5 = {
	{"","default:coal_lump",""},
	{"","default:stick",""},
	{"","",""}
}

table6 = {
	{"default:chest_locked","default:chest_locked","default:chest_locked"},
	{"default:sign","default:chest_locked","default:sign"},
	{"default:sign","default:torch","default:sign"}
}
]]--

--[[ pipes ]]--
table1 = { --pipes
	{"basic_materials:plastic_sheet","basic_materials:plastic_sheet","basic_materials:plastic_sheet"},
	{"","",""},
	{"basic_materials:plastic_sheet","basic_materials:plastic_sheet","basic_materials:plastic_sheet"}
}

table2 = { --succ
	{"default:sand","pipeworks:tube_1","default:sand"},
	{"","",""},
	{"","",""}
}

table3 = { --fragments
	{"","",""},
	{"","default:mese",""},
	{"","",""}
}

table4 = { --sorters
	{"pipeworks:tube_1","default:mese_crystal_fragment","default:mese_crystal_fragment"},
	{"default:mese_crystal_fragment","default:mese_crystal_fragment",""},
	{"","",""}
}

table5 = {
	{"","",""},
	{"","",""},
	{"","",""}
}

table6 = {
	{"","",""},
	{"","",""},
	{"","",""}
}

--[[ sticks and chests
table1 = {
	{"default:tree","",""},
	{"","",""},
	{"","",""}
}

table2 = {
	{"default:wood","",""},
	{"","",""},
	{"","",""}
}

table3 = {
	{"default:wood","default:wood","default:wood"},
	{"default:wood","default:wood","default:wood"},
	{"","default:stick",""}
}

table4 = {
	{"default:wood","default:wood","default:wood"},
	{"default:wood","","default:wood"},
	{"default:wood","default:wood","default:wood"}
}

table5 = {
	{"","",""},
	{"","",""},
	{"","",""}
}

table6 = {
	{"","",""},
	{"","",""},
	{"","",""}
}
]]--

--[[ template
table1 = {
	{","",""},
	{"","",""},
	{"","",""}
}

table2 = {
	{"","",""},
	{"","",""},
	{"","",""}
}

table3 = {
	{"","",""},
	{"","",""},
	{"","",""}
}

table4 = {
	{"","",""},
	{"","",""},
	{"","",""}
}

table5 = {
	{"","",""},
	{"","",""},
	{"","",""}
}

table6 = {
	{"","",""},
	{"","",""},
	{"","",""}
}
]]-

--[[
table3 = {
name = "default:wood",
count = 99
} ]]--


digiline_send("1", table1)
digiline_send("2", table2)
digiline_send("3", table3)
digiline_send("4", table4)
digiline_send("5", table5)
digiline_send("6", table6)

