platform = "" -- String for name of platform
services = "" -- Name of services using the network

firstdistance = S.data.first.distance
seconddistance = S.data.second.distance
thirddistance = S.data.third.distance
fourthdistance = S.data.fourth.distance


if S.data.first.line == nil then
    S.data.first.line = "works"
end
   
if S.data.second.line == nil then
    S.data.second.line = "works"
end
   
if S.data.third.line == nil then
    S.data.third.line = "works"
end

interrupt(1)

--[[
    Data:
    
    S.data.first{
            distance = distance,
            length = train_length(), 
            line = get_line(), 
            speed = atc_speed,
        }
]]--


header = "Platform " .. platform .. " - " .. services .. "\n"

train1 = "1st - " .. S.data.first.line .. ", " .. tostring(S.data.first.length) .. " cars,  " .. tostring(S.data.first.distance) .. " m" .. "\n" 
train2 = "2nd - " .. S.data.second.line .. ", " .. tostring(S.data.second.length) .. " cars,  " .. tostring(S.data.second.distance) .. " m" .. "\n" 
train3 = "3rd - " .. S.data.third.line .. ", " .. tostring(S.data.third.length) .. " cars,  " .. tostring(S.data.third.distance) .. " m" .. "\n" 


-- Time calculations

--[[
    Meaning:
    if distance in table < locally stored distance
        update time calulation to reflect that
    if not
        decrement time remaining by one
    end
]]


--[[if S.data.first.distance < firstdistance then
    ftime = S.data.first.distance / S.data.first.speed

    firstdistance = S.data.first.distance
else
    ftime = ftime-1
end

if S.data.second.distance < seconddistance then
    stime = S.data.second.distance / S.data.second.speed
    
    seconddistance = S.data.second.distance
else
    stime = stime-1
end

if S.data.third.distance < thirddistance then
    ttime = S.data.third.distance / S.data.third.speed
    
    thirddistance = S.data.third.distance
else
    ttime = ttime-1
end

if S.data.fourth.distance < fourthdistance then
    ftime = S.data.fourth.distance / S.data.fourth.speed
    
    fourthdistance = S.data.fourth.distance
else
    ftime = ftime-1
end

firsttime = "1st - " .. tostring(ftime) .. " s" .. "\n"
secondtime = "2nd - " .. tostring(stime) .. " s" .. "\n"
thirdtime = "3rd - " .. tostring(ttime) .. " s" .. "\n"
fourthtime =  "4th - " .. tostring(ftime) .. " s" .. "\n"
]]

-- Collecting variabls
display = train1 .. train2 .. train3
--time = firsttime .. secondtime .. thirdtime .. fourthtime

-- Writing to database
S.display.page1 = {
    display = display,
}

S.display.page3 = {
    display = "hi!" --time
}