placeTrack = POS(0,0,0) -- Co-ordinates for the track you are working on
placeStation = POS(0,0,0) -- Co-ordinates for a track of your choice in the station

distance = math.sqrt((placeTrack.x-placeStation.x)^2+(placeTrack.y-placeStation.y)^2+(placeTrack.z-placeStation.z)^2) -- Comment this out if using a different distance calculation and have a raw number
-- distance = xx -- Uncomment this and fill in your number in place of xx if you have a raw number

S.data = {
        first = {
            distance = 1000000000,
        },
        second = {
            distance = 1000000000,
        },
        third = {
            distance = 1000000000,
        },
        fourth = {
            distance = 1000000000,
        },
}

if event.type == "train" then
    data = {
        distance = distance,
        length = train_length(), 
        line = get_line(), 
        speed = atc_speed,
    }

    if distance < S.data.first.distance then -- Show information for closer trains before further trains
        S.data.first = data
    elseif distance < S.data.second.distance then
        S.data.second = data
    elseif distance < S.data.third.distance then
        S.data.third = data
    elseif distace < S.data.fourth.distance then
        S.data.fourth = {
            distance = distance,
            speed = atc_speed,
        }
    end
end