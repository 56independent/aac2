-- Note: this code assumes a constant speed, which helps avoid complexity. The times will be shorter then the real time.

distance = 0 -- Whatever your distance is
stopping_lines = {} -- Give a list of lines that stop  (empty for none)
channel = "1" -- Final channel for the message (defaults to 1)

prefix = "seconds"

-- DETECTION AND ACTING
if event.type == "train" then    
    --[[if #(stopping_lines) ~= 0 then
        if get_line ~= nil then
            for _, fruit in pairs(stopping_lines) do
                if fruit == get_line() then
                
                    -- CALCULATION
                    time = atc_speed/distance -- Use our delicate formula to get time, in seconds. Of course, on approach, we will be slowing down.
                    if time > 59 then
                        time = time/60
                        prefix = "minutes"
                    end
                    
                    -- OUTPUT
                    message = get_line() .. "\n" .. "In " .. time .. "\n" .. distance .. " metres away"
                    digiline_send(channel, message)
                end
            end
        else
            -- CALCULATION
            time = atc_speed/distance -- Use our delicate formula to get time, in seconds. Of course, on approach, we will be slowing down.
            if time > 59 then
                time = time/60
                prefix = "minutes"
            end
            
            -- OUTPUT
            message = get_line() .. "\n" .. "In " .. time .. "\n" .. distance .. " metres away"
            digiline_send(channel, message)
        end
    end]]--
    -- CALCULATION
    time = distance/atc_speed -- Use our delicate formula to get time, in seconds. Of course, on approach, we will be slowing down.
    if time > 59 then
        time = time/60
        prefix = "minutes"  
    end
    
    -- OUTPUT
    message = get_line() .. "\n" .. "In (very approximately)\n" .. time .. " " .. prefix .. ",\n" .. distance .. " metres away" .. "\nOf " .. train_length() .. " carriages"
    digiline_send(channel, message) 
end
