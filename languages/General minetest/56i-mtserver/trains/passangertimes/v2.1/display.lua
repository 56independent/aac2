channel = "p1" -- Channel of displays for specific platform. Allows station-wide digline systems.
platform = "" -- Platform number
services = "" -- Services for the platform in line names (maybe "IPR, NX2" for Re platform 1 on 56i-mtserver).
skippingrc = "cs" -- Put in the RC which means the train skips

interrupt(2) -- Set in environment code.

--[[

{distance = distance, length = length, line = line, rc = rc, skipping = skipping}

Table format for info

Platform 3 - EW services
30 m Away, 6 cars
EW line, RC By
Stopping here

Information shown

]]--
    
if F.tbl.rc == skippingrc then
    stopping = "Skipping Station. Stand back"
else
    stopping = "Stopping Here."
end
   

info = "Platform " .. platform .. " - " .. services .. " services" .. "\n" .. F.tbl.distance .. " m away, " .. F.tbl.length .. " cars" .. "\n" .. F.tbl.line .. " line, " .. F.tbl.rc .. "\n" .. stopping

digiline_send(channel, info)