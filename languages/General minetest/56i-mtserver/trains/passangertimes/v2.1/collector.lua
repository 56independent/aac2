placeTrack = POS(0,0,0) -- Co-ordinates for the track you are working on
placeStation = POS(0,0,0) -- Co-ordinates for a track of your choice in the station

distance = math.sqrt((placeTrack.x-placeStation.x)^2+(placeTrack.y-placeStation.y)^2+(placeTrack.z-placeStation.z)^2) -- Comment this out if using a different distance calculation and have a raw number
-- distance = xx -- Uncomment this and fill in your number in place of xx if you have a raw number

if event.type == "train" then
    if distance < F.tbl.distance then -- Show information for closer trains before further trains
        F.tbl = {
            distance = distance,
            length = train_length(), 
            line = get_line(), 
            rc = get_rc(), 
        }
    end
end