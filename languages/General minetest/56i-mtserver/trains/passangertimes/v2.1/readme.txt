This program aims to give information for trains in the following format:

Platform 3 - EW services
30 m Away, 6 cars
EW line, RC By
Stopping here

Using these:

* collector.lua - LuaATC tracks
* display.lua - LuaATC code controlling the displays

