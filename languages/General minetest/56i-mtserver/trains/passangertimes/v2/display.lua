channel = "p1" -- Channel of displays for specific platform. Allows station-wide digline systems.
platform = "" -- Platform number
services = "" -- Services for the platform in line names (maybe "IPR, NX2" for Re platform 1 on 56i-mtserver).


interrupt(F.time) -- Set in environment code.

--[[

{distance = distance, length = length, line = line, rc = rc, skipping = skipping}

Table format for info

Platform 3 - EW services
30 m Away, 6 cars
EW line, RC By
Stopping here

Information shown

]]--

if F.info[skipping] == false then
    stopping = "Stopping Here"
else
    stopping = "Skips platform"
end

    info = "Platform " .. platform .. " - " .. services .. " services" .. "\n" .. F.info[distance] .. " m away, " .. F.info.[length] .. " cars" .. "\n" .. F.info.[line] .. " line, " .. F.info[rc] .. "\n" .. stopping

digiline_send(channel, info)