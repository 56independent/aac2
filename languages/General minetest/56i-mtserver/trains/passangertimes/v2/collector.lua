placeTrack = POS(473,8,216) -- Co-ordinates for the track you are working on
placeStation = POS(473,8,) -- Co-ordinates for a track of your choice in the station

distance = math.sqrt((placeTrack.x-placeStation.x)^2+(placeTrack.y-placeStation.y)^2+(placeTrack.z-placeStation.z)^2) -- Comment this out if using a different distance calculation and have a raw number
-- distance = xx -- Uncomment this and fill in your number in place of xx if you have a raw number

if event.type == "train" then
    skippingrc = "cs" -- Put in the RC which means the train skips
    if F.get_rc_safe() == skippingrc then
        skipping = true
    else
        skipping = false
    end

    F.update({distance, train_length(), get_line(), F.get_rc_safe(), skipping})
end