global F.info = {}

function F.update(distance, length, line, rc, skipping)
    if distance < F.info.distance then -- Show information for closer trains before further trains
        F.info = {distance = distance, length = length, line = line, rc = rc, skipping = skipping}
    end
end

F.get_rc_safe = function()
	return get_rc() or ""
end

F.time = 2 -- Time in seconds between updates to display panels