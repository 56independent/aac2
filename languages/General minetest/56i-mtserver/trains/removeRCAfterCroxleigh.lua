--[[ Matches this data:

O b 4
S a 3
C c 1

x ^ ^ is what we need to replace with space 

We trim the last 4 characters. 

Except for branch line trains:

O 56i_s a 3

But "last 4" is relative.
]]--

if event.type == "train" then  
    rc = get_rc
    string.sub(rc, 4)
    set_rc(rc)
end

-- Should only be run after getting a station RC from the lua tracks before