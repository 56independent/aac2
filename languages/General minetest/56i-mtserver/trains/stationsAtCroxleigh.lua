track_number = 1 -- An odd number, which is the number of one track, but actually covers two.
passive_signal = "" -- A string which gives the name of the passive input component

--[[
P| |P| |P| |P| |P| |P| |P
P| |P| |P| |P| |P| |P| |P - a
P| |P| |P| |P| |P| |P| |P
P| |P| |P| |P| |P| |P| |P - b
P| |P| |P| |P| |P| |P| |P
P| |P| |P| |P| |P| |P| |P - c
P| |P| |P| |P| |P| |P| |P
--]]

-- This code can expose you to repetitive code, which is known to the State of California to cause cancer, birth defects, or other reproductive harm. 

if event.type == "train" the
    RC = get_rc()    

    routename = tostring(track_number) .. " a"
    rcname = routename
    if can_set_route(passive_signal, routename) then
        --set_route(passive_signal, routename)
        set_rc(rc .. rcname)
    end
    
    routename = tostring(track_number + 1) .. " a"
    rcname = routename
    if can_set_route(passive_signal, routename) then
        --set_route(passive_signal, routename)
        set_rc(rc .. rcname)
    end
    
    routename = tostring(track_number) .. " b"
    rcname = routename
    if can_set_route(passive_signal, routename) then
        --set_route(passive_signal, routename)
        set_rc(rc .. rcname)
    end
    
    routename = tostring(track_number + 1) .. " b"
    rcname = routename
    if can_set_route(passive_signal, routename) then
        --set_route(passive_signal, routename)
        set_rc(rc .. rcname)
    end
    
    routename = tostring(track_number) .. " c"
    rcname = routename
    if can_set_route(passive_signal, tostring(track_number)  .. " c") then
        --set_route(passive_signal, tostring(track_number) .. "c")
        set_rc(rc .. rcname)
    end
    
    routename = tostring(track_number + 1) .. "c"
    rcname = routename
    if can_set_route(passive_signal, tostring(track_number + 1)  .. "c") then
        --set_route(passive_signal, tostring(track_number + 1) .. "c")
        set_rc(rc .. rcname)
    end
end
