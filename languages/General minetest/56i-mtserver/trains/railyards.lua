signalPos = "neom" -- Hardcoded
lastRoute = "6" -- Hardcoded

if event.type == "train" then
 
 for i = lastRoute,1,-1 do
  l = tostring(i)
 	if can_set_route(signalPos, l) then
  		set_route(signalPos, "t" .. l)
  	end
  end
end
