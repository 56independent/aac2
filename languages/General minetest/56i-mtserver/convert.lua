--[[
    Inputs:
        kmhi
        msi
        mphi
    Outputs:
        kmh
        ms
        mph
]]

-- The converter uses four decimals of accuracy.

function convertAndSend(channel, factor, message)
    digiline_send(channel,tostring(tonumber(message)*factor) )
end

if event.type == "digilines" then
    if event.channel == "kmhi" then
        convertAndSend("kmh", 1, event.message)
        convertAndSend("ms", 0.2778, event.message)
        convertAndSend("mph", 0.6214, event.message)
    end
    if event.channel == "msi" then
        convertAndSend("kmh", 3.600, event.message)
        convertAndSend("ms", 1, event.message)
        convertAndSend("mph", 2.2369, event.message)
    end
    if event.channel == "mphi" then
        convertAndSend("kmh", 1.6093, event.message)
        convertAndSend("ms", 0.4470, event.message)
        convertAndSend("mph", 1, event.message)
    end
end
