--[[
{
	type = "item",
	pin = {name = <src dir>},
	itemstring = <itemstring>,
	item = <itemstack made to table>,
	velocity = <velocity of the tubed item>,
}

Event recap
]]--

function table.indexof(list, val)
	for i, v in ipairs(list) do
		if v == val then
			return i
		end
	end
	return -1
end

shop_name = "woodShop" -- Name of shop to send to, is id number (directory on wiki)
topcolour = "green" -- The colour atop the tube, to be sent to shop
sidecolour = "black" -- Colour item goes when rejected 

if event.type == "item" then
	item = event.item.name
	digiline_send(shop_name, item)
end

if event.type == "digiline" then
		--[[if (event.msg == "yes") and (event.channel == "main2" .. shop_name) then -- verifies it is the source controller and checks for "yes".
			return topcolour;
		else
			return sidecolour;
		end]]--
		
        items = event.msg
	    if table.indexof(items, event.msg) ~= -1 then
	        digiline_send(shop_name.."slave", topcolour)
	    else
            digiline_send(shop_name.."slave", sidecolour)
	    end
end
