-- Lua code for the pipes which input material

function table.indexof(list, val)
	for i, v in ipairs(list) do
		if v == val then
			return i
		end
	end
	return -1
end

shopColour = "" -- Colour which sends items to shops
rejectColour = "" -- If item is rejected, the colour it goes to

if event.type == "item" then
    digiline_send("input", event.itemstring)
end

if (event.type == "digiline") and (event.channel == "main2input") then
        items = event.msg
        
        if  (table.indexof(items, event.msg) ~= -1) then
            digiline_send("injector", {name = "currency:minegeld", 10}) -- 10 mg for all
            digiline_send("injectorslave", shopColour)
        else
            digiline_send("injectorslave", rejectColour)
        end
end
