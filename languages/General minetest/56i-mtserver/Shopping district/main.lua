-- Main luacontroller

-- Function
function table.indexof(list, val)
	for i, v in ipairs(list) do
		if v == val then
			return i
		end
	end
	return -1
end

-- The big table
items = {
	woodShop = { -- Name of shop
		"default:wood"  -- List of items available
	},
	otherShop = {
		"default:glass"
	}
}

-- Handles logic

if event.type == "digiline" then
    --digiline_send("main2" .. event.channel, items)
	if event.channel == "input" then
		digiline_send("main2input", items)	
	else 
	    --[[name = event.channel
	    if table.indexof(items, event.msg) ~= -1 then
	        digiline_send("main2" .. event.channel, "yes")
	    else
	        digiline_send("main2" .. event.channel, "no")
	    end --]]
	    
	    digiline_send("main2" .. event.channel, items[event.channel])
	end
end
