-- writer

-- channels 
--- card reader - crd
--- lcd - out

if event.type == "digiline" then
	digiline_send("crd",{command = "write",data = "door_open_vertified", description = "allowed to access secret room"})
	digiline_send("out","100 deposited")
end

-- reader

-- channels 
--- card reader - crd
--- lcd - out

if event.type == "digiline" then
	if event.msg.data == "100" then
		digiline_send("out","purchase succesful")
		digiline_send("crd",{command = "write",data = "door_open_unverified", description = "rescan please"})
	else
		digiline_send("out","not enough, sorry")
	end
end

--[[
After setting a channel, swiping a card (punch the reader with the card to swipe) will send a message in the following format:
	{event = "read",data = "The data that was on the card"}
To write a card, send a command in the following format:
	{command = "write",data = "The data to put on the card",description = "A description of what the card is for"}
After sending the write command, swipe the card to be written and the reader will send back the following message:
	{event = "write"}
--]]
