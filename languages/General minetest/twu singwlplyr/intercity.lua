
-- sleep function (needs timer thingywingy)
function sleep(time)
	digiline_send("timer",time)
	::here::
	if (event.channel == "timer") and (event.msg == "done" then 
	else 
	goto here
	end
end

-- writer

-- channels 
--- card reader - crd
--- lcd - out

if event.type == "digiline" then
	digiline_send("crd",{command = "write",data = "intercity_vertified", description = "intercity access"})
	digiline_send("out","pass made!")
	sleep(5)
	  digiline_send("out","")
	  port.c = false
end

-- reader

-- channels 
--- card reader - crd
--- lcd - out

if event.type == "digiline" then
	if event.msg.data == "intercity_vertified" then
		digiline_send("out","vertified")
	else
		digiline_send("out","unvertified. try getting a new card.")
	end
	sleep(5)
	  digiline_send("out","")
	  port.c = false
end

--[[
After setting a channel, swiping a card (punch the reader with the card to swipe) will send a message in the following format:
	{event = "read",data = "The data that was on the card"}
To write a card, send a command in the following format:
	{command = "write",data = "The data to put on the card",description = "A description of what the card is for"}
After sending the write command, swipe the card to be written and the reader will send back the following message:
	{event = "write"}
--]]
