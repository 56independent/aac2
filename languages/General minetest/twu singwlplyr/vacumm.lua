-- a simple program to simulate vacumms

-- io:
--- button - input, with messages
--- port.c - the door controller
--- status - output, for outputting status

mem.vacumm = false 

if event.type == "digiline" then
	if event.msg == "s/p" then
		if mem.vacumm == false then
		digiline_send("status","closing doors") 
		port.c = false
		-- wait for 1 second
		digiline_send("status","sucking...")
		-- wait for 10 seconds
		mem.vacumm = not mem.vacumm
		digiline_send("status","vacumm")
		
		elseif mem.vacumm == true then
		digiline_send("status","allowing air back in")
		-- wait for 5 seconds
		mem.vacumm = false
		digiline_send("status","opening doors")
		-- wait 1 second
		digiline_send("status","no vacumm")
		end
	end
end

