print("program started!")
#a piano, made with tkinter. uses winsound, and the beep duration is 0.75 seconds by default

#notes:
#todo: extend the note range, to be between c1/c2 and c6-c8

#the modules:
import tkinter as tk
from winsound import Beep
print("imported exotic modules (posh posh)")
#functions (oh no)
def pc4():
    Beep(262,750) # plays c4, rounded up 261.63 to 262, since beep needs a int, not  a float. p for play.
def pc4s():
    Beep(277,750) # s is for sharp (sorry flats, but i had to choose one)
def pd4():
    Beep(294,750)
def pd4s():
    Beep(311,750)
def pe4():
    Beep(330,750)
def pf4():
    Beep(349,750)
def pf4s():
    Beep(370,750)
def pg4():
    Beep(392,750) # ahh, according to my resource, the frequency for g is exactly 392.00hz. no need for rounding
def pg4s():
    Beep(415,750)
def pa4():
    Beep(440,750) # wait, what?!its exactly 440hz! what next, exactly 400hz?!
def pa4s():
    Beep(466,750)
def pb4():
    Beep(494,750)
def pc5():
    Beep(523,750)
print("defined all functions")
#window, and window name
piano = tk.Tk()
piano.title("piano")
print("made the window!")
#instructions
instructions = tk.Label(
    text = "press any button to play the note. the default play time is 0.75 seconds ,\n or 750 miliseconds. winsound.Beep dosent accept any foats (0.6764),\n so i ahev to roun them up (1). c4 is actually 261.63 hz,\n but i rounded it up to 262 :-/"
)
print("made instructions label")
#all the needed buttons, from c4 to c5 (i know, this is very amuteur. but i am a 14y/o coder, trying out something new.

c4 = tk.Button(
    text = "c4",
    command = pc4
)
c4s = tk.Button(
    text = "c4#/d4f",
    command = pc4s
)
d4 = tk.Button(
    text = "d4",
    command = pd4
)
d4s = tk.Button(
    text = "d4#/e4f",
    command  =pd4s
)
e4 = tk.Button(
    text ="e4",
    command = pe4
)
f4 = tk.Button(
    text ="f4",
    command = pf4
)
f4s = tk.Button(
    text = "f4#/gf",
    command = pf4s
)
g4 = tk.Button(
    text = "g4",
    command = pg4
)
g4s = tk.Button(
    text = "g4#/af",
    command = pg4s
)
a4 = tk.Button(
    text = "a4",
    command = pa4
)
a4s = tk.Button(
    text = "a4#/bf",
    command = pa4s
)
b4 = tk.Button(
    text = "b4",
    command = pb4
)
c5 = tk.Button(
    text = "c5",
    command = pc5
)
print("made all the note buttons!")
#places buttons+labels on screen
#i know the instructions are on the bottom, but otherwise the .grid will make the column too wide :-/
instructions.place(y=50)
c4.grid(row=3,column=1)
c4s.grid(row=2,column=1)
d4.grid(row=3,column=2)
d4s.grid(row=2,column=2)
e4.grid(row=3,column=3)
f4.grid(row=3,column=4)
f4s.grid(row=2,column=4)
g4.grid(row=3,column=5)
g4s.grid(row=2,column=5)
a4.grid(row=3,column=6)
a4s.grid(row=2,column=6)
b4.grid(row=3,column=7)
c5.grid(row=3,column=8)
print("placed the buttons!")
piano.mainloop()
print("finished!")
