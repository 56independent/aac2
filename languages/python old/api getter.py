#import modules
import tkinter as tk
#import a module that is NEEDED for getting title and description from wikidata
import qwikidata

#create some windows
wikiwindow = tk.Tk()
wikiwindow.title("wikidata api client (or whatever)")

#functions
def enterfindanddisplay():
	qstr = enterq.get()
	link = "https://www.wikidata.org/wiki/Q"+qstr
	#we have the link, lets place it inside our label
	linklabel["text"] = link

#creates some buttons, as well as labels
##column 1
enterq = tk.Entry()
buttonq = tk.Button(
	text = "enter q number",
	command = enterfindanddisplay
	)

##column 2
title = tk.Label()
description = tk.Label()
linklabel = tk.Label()

#widget placers
enterq.grid(row=1,column=1)
buttonq.grid(row=2,column=1)

title.grid(row=1,column=2)
description.grid(row=2,column=2)
linklabel.grid(row=3,column=2)

#extra stuff (execute functions, set variables)
enterq.insert(0,"enter a q number for wikidata")
wikiwindow.mainloop()