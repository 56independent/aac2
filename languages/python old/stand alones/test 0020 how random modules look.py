print("how the rand modules look")
from turtle import *
from random import randint
from secrets import randbelow
exits = False
while not exits == True:
    print("what module would you like to see? (r)andom, or (s)ecrets or (h)elp")
    mode = input("")
    if mode == "h":
        print("this is for randoms, the length is controled by the module, but the angle is a solid 45 degrees.")
    while mode == "r":
        forward(randint(0,75))
        angledir = randint(1,2)
        if angledir == 1:
            left(45)
        if angledir == 2:
            right(45)
    while mode == "s":
        forward(randbelow(75))
        angledir = randbelow(2)
        if angledir == 1:
            left(45)
        if angledir == 2:
            right(45)