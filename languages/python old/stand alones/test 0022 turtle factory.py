#factory
from turtle import *
from time import sleep
penup()
goto (-750,-350)
colour = input("what colour do you want as the line colour? (hashes (#A23B23) and normals (green) are accepted)")
pencolor(colour)
choice3 = input("(l)ine or (n)o line?")
if choice3 == "l":
    pendown()
choice = input("(o)utline or (n)o outline?")
skip = int(input("how much pixels up every time the edge is hit?"))
speed(0)
while True:
    forward(20)
    if choice == "o":
        stamp()
    num = xcor()
    if num>750:
        left(90)
        forward(skip)
        left(90)
    if num<-750:
        right(90)
        forward(skip)
        right(90)
    num2 = ycor()
    if num2 > 350:
        sleep(5)
        clear()
        penup()
        goto(-750,-350)
        if choice3 == "l":
            pendown()
input("thanks for using!")
