#conf reader
#notes
## todo: add more buttons
## todo: make sure settings arent in sepeate files, and within ONE FUCKING MOTHERFILE TO REPLACE THE SEPERATE ONES

#modules
import tkinter as tk

#windows
reader = tk.Tk()
reader.title("settings")

#functions
def refresh():
    #if the file dosent exist, it throws an error. if it happens, sweep under the rub and do what is in except.
    try:
        gravity = open("settingsstore/gravity.txt","r")
        howgravity["text"] = gravity.read()
        gravity.close()
    except:
        gravity = open("settingsstore/gravity.txt","w")
        gravity.close()
        
    try:
        walkspeed = open("settingsstore/walkspeed.txt","r")
        howwalkspeed["text"] = walkspeed.read()
        walkspeed.close()
    except:
        gravity = open("settingsstore/walkspeed.txt","w")
        gravity.close()
def setgravity():
    gravity = open("settingsstore/gravity.txt","w")
    gravity.write(gravityentry.get()) # sends the data from gravityentry into a file, containing the info
    gravity.close()
def setwalkspeed():
    walkspeed = open("settingsstore/walkspeed.txt","w")
    walkspeed.write(walkspeedentry.get())
    walkspeed.close()

#widgets
##column 1
howgravity = tk.Label()
gravityentry = tk.Entry()
setgravity = tk.Button(
    text = "set gravity value",
    command = setgravity
    )
                     
##column 2
howwalkspeed = tk.Label()
walkspeedentry = tk.Entry()
setwalkspeed = tk.Button(
    text = "set walkspeed value",
    command = setwalkspeed
    )
                     
##column 10
refresh = tk.Button(
    text = "refresh",
    command = refresh
    )

#places widgets
##column 1
howgravity.grid(row=1,column=1)
gravityentry.grid(row=2,column=1)
setgravity.grid(row=3,column=1)

##column 2
howwalkspeed.grid(row=1,column=2)
walkspeedentry.grid(row=2,column=2)
setwalkspeed.grid(row=3,column=2)

##column 10
refresh.grid(row=1,column=10)

reader.mainloop()