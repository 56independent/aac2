#notes:
## todo: add a small counter to update, so that a figure only randomises every n updates.
## todo: more clicky buttons
## todo: more labels and figures
## todo: comments! there should be grey on the outside of EVERY STATEMENT.
## todo: remove THE FUCKING SQUARE BRACKETS ON THE "generatorsonline" LABEL!!!!
## todo: make more detailed generator info, with generating power, 
## todo: add a timeout, so you have to wait 5 seocnds before pressing the button again. the Kw shouldnt change by 50Kw in only 2 seconds
## possible fix: scan through the string, remove any [ or ] inside the string.

#lets import modules!
import tkinter as tk #imports tkinter, for the ui
from random import randint #gets random numbers ready
import os #allows for deleting of files
from time import sleep #allows for artifical, calculatable delay.
#import winsound # to scare the crap out of people when a siren comes one or sommin

#functions
def update(): #defines a new funtion
    #class generator:
    #    def .__init__(self,number,generation,active):
    #        self.number = number
    #        self.generation = generation
    #        self.active = active
    #    def activate(self):
    
    #note before you read. i know its brute forced, i know its poor programming, but it works!

    #lets read the values from the files!
    reactor1f = open("reactor1","r")
    reactor1 = reactor1f.read()
    reactor1f.close() #neatly opens, reads and closes the file for reactor1.

    reactor2f = open("reactor2","r")
    reactor2 = reactor2f.read()
    reactor2f.close()

    reactor3f = open("reactor3","r")
    reactor3 = reactor3f.read()
    reactor3f.close()

    if reactor1 == "active":
        reactor1generation = randint(45,65)
        reactbut1["text"] = "shutdown"
        reactbut1["command"] = shut1
        status1["text"] = "active"
    elif reactor1 == "off":
        reactor1generation = 0 #yes, a big fat zero, for NOT BEING ACTIVE!!!
        reactbut1["text"] = "activate"
        reactbut1["command"] = act1
        status1["text"] = "off"

    if reactor2 == "active":
        reactor2generation = randint(45,65)
        reactbut2["text"] = "shutdown"
        reactbut2["command"] = shut2
        status2["text"] = "active"
    elif reactor2 == "off":
        reactor2generation = 0
        reactbut2["text"] = "activate"
        reactbut2["command"] = act2
        status2["text"] = "off"

    if reactor3 == "active":
        reactor3generation = randint(45,65) # these generate random numbers...
        reactbut3["text"] = "shutdown"
        reactbut3["command"] = shut3
        status3["text"] = "active"
    else:
        reactor3generation = 0 #another FAT 0! (wow, these reactors are really lazy)
        reactbut3["text"] = "activate"
        reactbut3["command"] = act3
        status3["text"] = "off"
        
    electricity = reactor3generation+reactor1generation+reactor2generation #ensure the output is equal to the generation of each reactor

    strelectricity = str(electricity) # turns eectricity into a string by the name strelectricity
    demand = electricity-randint(-5,5) # demand value, with difference form electricity ebing from -5 to 5
    if demand<0:
        #what the hell?! demand is a minus number?! lets fix it (fixing noises)
        demand = 1
    strdemand = str(demand) #turns demand into a string
    figureofgen["text"] = f"{strelectricity} Kw" # puts the energy generation inside
    demandabel["text"] = f"{strdemand} Kw" # puts the demand inside its label
    stationsonline["text"] = f"{randint(15,18)}" #puts online stations inside
    
    strreactor1generation = str(reactor1generation)
    strreactor2generation = str(reactor2generation)
    strreactor3generation = str(reactor3generation)
    
    
    reactor1gen["text"] = strreactor1generation+" Kw"
    reactor2gen["text"] = strreactor2generation+" Kw"
    reactor3gen["text"] = strreactor3generation+" Kw"

    
def save(): 
    # saving function, for saving figures from the top. works by reading the labels                
    filename = entryforsavebutton.get()
    generatedelec = figureofgen["text"]#grabs figures (hopefully)
    demand = demandabel["text"]
    station = stationsonline["text"]
    #station = str(station) #remember, this sint a string, so has to be converted. (or maybe formatted strings convert for you?)
    #generator = str(generator) #commennted out old code. to reactivate, remove the "#"
    fig = open(filename + ".txt","w")
    try:
        fig.write("generated electricity = " + generatedelec )
        fig.write("\ndemand = " + demand)
        fig.write("\nstations online = " + station)
        fig.write("\ngenerators online= " + generator)
    finally:
        fig.close()
        
def startmelt():
    def passcheck():
        password = enterpass.get()
        if password == "password" or password == "1234" or password == "enter password here" or password == "12345678":
            #its time to start the meltdown!
            generatortodestroy = randint(1,3)
            if generatortodestroy == 1:
                turnreactorsoff()
                reactbut1["state"] = "disabled"
                reactbut2["state"] = "disabled"
                reactbut3["state"] = "disabled"

                status1["text"] = "MELTDOWN"
                status1["bg"] = "red"
                reactbut1["bg"] = "red"
            if generatortodestroy == 2:
                turnreactorsoff()
                reactbut1["state"] = "disabled"
                reactbut2["state"] = "disabled"
                reactbut3["state"] = "disabled"

                status2["text"] = "MELTDOWN"
                status2["bg"] = "red"
                reactbut2["bg"] = "red"
            if generatortodestroy == 3:
                turnreactorsoff()
                reactbut1["state"] = "disabled"
                reactbut2["state"] = "disabled"
                reactbut3["state"] = "disabled"

                status3["text"] = "MELTDOWN"
                status3["bg"] = "red"
                reactbut3["bg"] = "red"

            #letes scare the crap out of people!
            winsound.PlaySound("siren1.wav",winsound.SND_ALIAS)
            winsound.PlaySound("speech.wav",winsound.SND_ALIAS)

            
    submitpass = tk.Button(
        text = "submit password",
        command = passcheck
        )
    enterpass = tk.Entry()
    
    enterpass.insert(0,"enter password here")
    
    enterpass.grid(row=5,column=10)
    submitpass.grid(row=6,column=10)

def shut1():
    #shuts down reactor one
    react = open("reactor1","w")
    react.write("off")
    react.close()
    update() # says "hey, can u update me figures <3 <3 luv uuuu"
def act1():
    react = open("reactor1","w")
    react.write("active")
    react.close()
    update() #says "ohh look at me, im soo turned onnn update me pls!"

def shut2():
    react = open("reactor2","w")
    react.write("off")
    react.close()# "no. no lov3  for me, im sleeping!"
    update()
def act2():
    react = open("reactor2","w")
    react.write("active")
    react.close()
    update()

def shut3():
    react = open("reactor3","w")
    react.write("off")
    react.close()
    update()
def act3():
    react = open("reactor3","w")
    react.write("active")
    react.close()
    update()

def turnreactorsoff():
    #open the file, overwrite and get them to turn off. close the file afterwards (very important)
    reactor1 = open("reactor1","w")
    reactor1.write("off")
    reactor1.close()

    reactor2 = open("reactor2","w")
    reactor2.write("off")
    reactor2.close()

    reactor3 = open("reactor3","w")
    reactor3.write("off")
    reactor3.close()

def shutdown():
    #just removes the files (storing some nucular reactor variables), some other crap as well
    shutdownbut["text"] = "shutting down..."
    if os.path.exists("reactor1"):
        os.remove("reactor1")
    if os.path.exists("reactor2"):
        os.remove("reactor2")
    if os.path.exists("reactor3"):
        os.remove("reactor3")
    exit("user clicked exit button")


#windows (would you beive i forgot this on the first run?!)
panel = tk.Tk() #creates a window
panel.title("nucuar power plant control panel") #hanges title of window



#widgets
##column 1
###rows 1-2
electricitygen = tk.Label( #creates the label for electricity generation
    text = "electricity generated:" #adds text
    ) 
figureofgen = tk.Label() #creates a label, ready to be filled
###rows 3-?
reactorlabel1 = tk.Label(
    text = "reactor 1"
    )
reactor1gen = tk.Label()
status1 = tk.Label()
reactbut1 = tk.Button()

##column 2
explanationofdemandlabel = tk.Label(
    text = "demand"
    )
demandabel = tk.Label()
###rows 3-?
reactorlabel2 = tk.Label(
    text = "reactor 2"
    )
reactor2gen = tk.Label()
status2 = tk.Label()
reactbut2 = tk.Button()

##column 3
explanationofstations = tk.Label(
    text = "power stations online"
    )
stationsonline = tk.Label()

###rows 3-?
reactorlabel3 = tk.Label(
    text = "reactor 2"
    )
reactor3gen = tk.Label()
status3 = tk.Label()
reactbut3 = tk.Button()

##column 10
updatebutton = tk.Button(
    text = "update",
    command = update,
    pady = 5
    )
savebutton = tk.Button(
    text = "save figures",
    command = save,
    pady=5
    )
entryforsavebutton = tk.Entry()

firstmelt = tk.Button(
    text = "start meltdown",
    command = startmelt,
    pady=5
    )

##column 11
shutdownbut = tk.Button(
    text = "shut down program",
    command = shutdown
    )

#place widgets
electricitygen.grid(row=1,column=1)
figureofgen.grid(row=2,column=1)
reactorlabel1.grid(row=3,column=1)
reactor1gen.grid(row=4,column=1)
status1.grid(row=5,column=1)
reactbut1.grid(row=8,column=1)

explanationofdemandlabel.grid(row=1,column=2)
demandabel.grid(row=2,column=2)
reactorlabel2.grid(row=3,column=2)
reactor2gen.grid(row=4,column=2)
status2.grid(row=5,column=2)
reactbut2.grid(row=8,column=2)

explanationofstations.grid(row=1,column=3)
stationsonline.grid(row=2,column=3)
reactorlabel3.grid(row=3,column=3)
reactor3gen.grid(row=4,column=3)
status3.grid(row=5,column=3)
reactbut3.grid(row=8,column=3)

updatebutton.grid(row=1,column=10)
savebutton.grid(row=2,column=10)
entryforsavebutton.grid(row=3,column=10)
firstmelt.grid(row=4,column=10)

shutdownbut.grid(row=1,column=11)

turnreactorsoff() # ensures that the files are written, before they get read (Potential error, BEGONE!)
update() # just gives it figures to start with, so i dont have to press the update button.
entryforsavebutton.insert(0,"add a filename here") # sets the text
panel.mainloop() # sets to keep it from "flashing" away in the python launcher
