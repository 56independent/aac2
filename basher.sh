#!/bin/bash

# sorry users of "bettercomment" vscode extension! you have to live with it srry!

# this is a program to:
# 1 - ask for a name
# 2 - add that name to a variable
# 3 - make a file with that name, filled with bash stuff
# 4 - mark as an executable

echo "name of ur bash file? (dont add .sh)"
read name

filename="./${name}.sh"

touch $filename
echo $'#!/bin/bash\n\n# sorry users of "bettercomment" vscode extension! you have to live with it srry!\n\n# this is a program to: \n# 1 - \n# 2 - \n\n# code here' >> $filename # sorry for the mesy newlines-in-one-line thing

chmod +x $filename
